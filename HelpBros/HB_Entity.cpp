#include "HB_Entity.h"

std::vector<HB_Entity* > HB_Entity::EntitiesPool;

HB_Entity::HB_Entity(int Width, int Height, HB_Map *mapReference)
{
	// Set the default parameters
	y = x = 100;
	
	width = Width;
	height = Height;

	movingLeft = false;
	movingRight = false;

	type = ENTITY_TYPE_GENERIC;

	jumpKeyReleased = true;
	canJump = false;
	dead = false;
	flags = ENTITY_FLAG_GRAVITY;

	xVel = yVel = 0;

	xAcc = yAcc = 0;

	xAccFactor = 1;

	xDecFactor = 1.5;

	maxVelX = maxVelY = 5;

	offsetX = offsetY = offsetW = offsetH = 0;

	idle = true;

	falling = jumping = onGround = false;

	collidedLR = 2;

	HP = 0;

	entityARect = new SDL_Rect();
	entityBRect = new SDL_Rect();
	
	lastDirection = DIRECTION_RIGHT;

	//collisions = new HB_Collisions();
	

	// Create the collisions object
	//collisions = new HB_Collisions(this);

	// Set the smart pointer to the animation class
	//anim = boost::shared_ptr<HB_Animation>(new HB_Animation());

	//anim->init(spritePath, spriteFrames, Width, Height, &facing, &x, &y);

	this->mapReference = mapReference;

	dyingAnimTimer = new HB_Timer();
	dyingAnimTime = 0;

	boltedAnimTimer = new HB_Timer();
	boltedAnimTime = 1000;
	

	state = ENTITY_STATE_ALIVE;

	prevState = ENTITY_STATE_ALIVE;
}

HB_Entity::~HB_Entity()
{
	//delete collisions;
	delete entityARect;
	delete entityBRect;
	delete dyingAnimTimer;
	delete boltedAnimTimer;
}

void HB_Entity::handle_events(SDL_Event *Event)
{

}

void HB_Entity::update(double *deltaTicks)
{
	// Once the dying animation time has passed
	if(dyingAnimTimer->get_ticks() >= dyingAnimTime)
	{
		dead = true;
		dyingAnimTimer->stop();
		state = ENTITY_STATE_DEAD;
	}

	// Once the bolting animation time has passed
	if(boltedAnimTimer->get_ticks() >= boltedAnimTime)
	{
		boltedAnimTimer->stop();
		state = ENTITY_STATE_ALIVE;
		flags = ENTITY_FLAG_GRAVITY;
		setAnimBack = true;
	}



	// If the entity has not been killed and it's playing the dying animation
	if(state != ENTITY_STATE_DYING)
	{
		

		// If the entity is not moving
		if(movingLeft == false && movingRight == false)
		{
			// Decellerate and stop
			stop_moving();
		}

		// If the entity is moving left
		if(movingLeft)
		{
			xAcc = -xAccFactor;
		}
		// If the entity is moving rigth
		else if(movingRight)
		{
			xAcc = xAccFactor;
		} 

		// If the entity is being affected by gravity
		if(flags & ENTITY_FLAG_GRAVITY)
		{
			yAcc = 1.35f;
		}

		// Accellerate the entity, setting the velocity
		xVel += xAcc * ((*deltaTicks / 1000.f) * PIXEL_PER_SECOND);
		yVel += yAcc * ((*deltaTicks / 1000.f) * PIXEL_PER_SECOND);

		// Cap the velocity
		if(xVel > maxVelX) xVel = maxVelX;
		if(xVel < -maxVelX) xVel = -maxVelX;
		if(yVel > maxVelY) yVel = maxVelY;
		if(yVel < -maxVelY) yVel = -maxVelY;


		// If the entity is firing, or bolting or crouching, it can't move
		if(state == ENTITY_STATE_FIRING || state == ENTITY_STATE_BOLTING || state == ENTITY_STATE_CROUCHING
			|| state == ENTITY_STATE_BOLTED)
		{
			movingLeft = movingRight = false;
			xVel = 0;
		}

		// If the entity falls down
		if(y > mapReference->heightPX)
		{
			// Make it die
			HP = 0;
		}

		// If the entity has finished its health points
		if(HP <= 0)
		{
			// Make it die
			dead = true;
		}

		move(deltaTicks);
	}

}

void HB_Entity::draw(SDL_Surface *destination)
{
	
}

bool HB_Entity::on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis)
{
	if(*axis == AXIS_X)
	{
		// Set the X velocity to 0 (stop immediately)
		xVel = 0;

		// Set the position where to move to 0, meaning the entity does not have to move anymore
		this->moveX = 0;
	}

	if(*axis == AXIS_Y)
	{
		// If the entity is on the ground
		if(moveY > 0)
		{
			canJump = true;
		}

		// Set the Y velocity to 0 (stop immediately)
		yVel = 0;

		// Set the position where to move to 0, meaning the entity does not have to move anymore
		this->moveY = 0;
	}

	return true;
}

bool HB_Entity::on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *axis)
{
	if(*axis == AXIS_X)
	{
		// Set the X velocity to 0 (stop immediately)
		xVel = 0;

		// Set the position where to move to 0, meaning the entity does not have to move anymore
		this->moveX = 0;
	}

	if(*axis == AXIS_Y)
	{
		// If the entity is on the ground
		if(moveY > 0)
		{
			canJump = true;
		}

		// Set the Y velocity to 0 (stop immediately)
		yVel = 0;

		// Set the position where to move to 0, meaning the entity does not have to move anymore
		this->moveY = 0;
	}
	return false;
}


void HB_Entity::stop_moving()
{
	// If the entity is moving right
	if(xVel > 0)
	{
		xAcc = -xDecFactor;
	}
	// If the entity is moving left
	if(xVel < 0)
	{
		xAcc = xDecFactor;
	}

	// If it is time to stop the entity
	if(xVel < 2.0f && xVel > -2.0f)
	{
		xAcc = 0;
		xVel = 0;
	}
}

bool HB_Entity::jump()
{
	if(canJump == false)
	{
		return false;
	}

	yVel = -30;

	//canJump = false;

	return true;
}

void HB_Entity::hurt(int HP)
{
	this->HP -= HP;
}

void HB_Entity::move(double *deltaTicks)
{
	
	
	// Obtain where the enetity needs or wants to move
	moveX = xVel;
	moveY = yVel;
	
	// Calculate the speed factor
	speedFactor = (*deltaTicks / 1000) * PIXEL_PER_SECOND;

	canJump = false;

	// If the entity is moving
	//if(moveX != 0 || moveY != 0)
	//{
		// Variables holding the speed factor (pixel-per-step)
		ppStepX = 0;
		ppStepY = 0;

		// Calculate the destination based on the speed factor
		moveX *= speedFactor;
		moveY *= speedFactor;

		// If the entity is moving on the X axis
		if(moveX != 0)
		{
			// If the entity is moving rightward
			if(moveX > 0)
			{
				// Set how much does each step takes in pixel
				ppStepX = speedFactor;
			}
			// If the entity is moving leftward
			else
			{
				// Set how much does each step takes in pixel
				ppStepX = -speedFactor;
			}
		}

		// If the entity is moving on the Y axis
		if(moveY != 0)
		{
			// If the entity is moving downward
			if(moveY > 0)
			{
				// Set how much does each step takes in pixel
				ppStepY = speedFactor;
			}
			// If the entity is moving upward
			else
			{
				// Set how much does each step takes in pixel
				ppStepY = -speedFactor;
			}
		}

		/* While the motion has not finished (moveX = 0, as it is going
			to decrease until it reaches zero; that way, the entity
			is going to move for the desired distance, but checking for
			each pixel in front of him if it is possible to go there)*/
		//if(state != ENTITY_STATE_BOLTED)
		
			while(true)
			{
				// If the entity is not affected by collisions
				if(flags & ENTITY_FLAG_GHOST)
				{
					/* In this case, to check for collisions to the next step is not 
						necessary, but it must be told to the other entities about 
						events from this one */
					pos_valid((x + ppStepX), (y + ppStepY), AXIS_Y);
				
					// Move the entity one step
					x += ppStepX;
					y += ppStepY;
				}
				// If the entity is affected by collisions
				else
				{
					//if(moveY != 0)
					//{
						/* If the position where the entity has to move, onto the Y 
							axis, this step is valid (if it can go there) */
						if(pos_valid((x), (y + ppStepY), AXIS_Y))
						{
							// Move the entity one step
							y += ppStepY;
						}
						else
						{
							// If the entity is on the ground
							if(moveY > 0)
							{
								canJump = true;
							}
						}
					//}
					//if(moveX != 0)
					//{
						/* If the position where the entity has to move, onto the X 
							axis, this step is valid (if it can go there) */
						if(pos_valid((x + ppStepX), (y), AXIS_X))
						{
							// Move the entity one step
							x += ppStepX;
						}
					//}
				}

				// Decrease the remaining distance to move one step
				moveX -= ppStepX;
				moveY -= ppStepY;


				// Check if the movement is done, then break the hell out of this loop

				// X Axis
				if(ppStepX > 0 && moveX <= 0) ppStepX = 0; // If movement rightward is done
				if(ppStepX < 0 && moveX >= 0) ppStepX = 0; // If movement leftward is done
				// Y Axis
				if(ppStepY > 0 && moveY <= 0) ppStepY = 0; // If movement downward is done
				if(ppStepY < 0 && moveY >= 0) ppStepY = 0; // If movement upward is done
				// If the function was called but the entity didn't have to move
				if(moveX == 0) ppStepX = 0;
				if(moveY == 0) ppStepY = 0;
				// If it is time to exit the loop (movements done)
				if(moveX == 0 && moveY == 0) 
				{
					break;
				}
				if(ppStepX == 0 && ppStepY == 0) break;
			}
		//}
	
}

bool HB_Entity::pos_valid(float stepX, float stepY, int axis)
{
	// Get the area of tiles of where the entity is willing to move to
	startX = (stepX + offsetX) / TILE_SIZE; 
	startY = (stepY + offsetY) / TILE_SIZE; 
	endX = (stepX + width -1 - offsetW) / TILE_SIZE;
	endY = (stepY + height -1 - offsetH) / TILE_SIZE;

	/* Loop trough each tile and check if one of them does not allow the entity
		on him */
	for(int iY = startY; iY <= endY; iY ++)
	{
		for(int iX = startX; iX <= endX; iX ++)
		{
			// Get the tile under examination at the moment
			HB_Tile *tempTile = mapReference->get_tile(iX * TILE_SIZE, iY * TILE_SIZE);

			// Check if the entity can move on it
			if(pos_valid_tile(tempTile) == false)
			{
				// Add a collision event to the poll
				HB_MapCollisions *collision = new HB_MapCollisions(this, tempTile, moveX, moveY, axis);
				HB_MapCollisions::mapColPoll.push_back(collision);

				return false;
			}
		}
	}

	// If this entity collides with the map only, skip the entities collision check
	if(flags & ENTITY_FLAG_MAPONLY)
	{
		// Do freakin' nothing
	}
	else
	{
		// Loop for each entity until a collision is found or no one is found
		for(int i = 0; i < EntitiesPool.size(); i++)
		{
			// If the entity collided with another one
			if((pos_valid_entity(this, EntitiesPool[i], &stepX, &stepY, axis)) == false)
			{
				return false;
			}
		}
	}

	

	return true;
}

bool HB_Entity::collision_check(HB_Entity *entityB, float *stepX, float *stepY)
{
	// Set the left bounds of the entities
	entityARect->x = (int)offsetX + *stepX;
	entityBRect->x = (int)entityB->x + entityB->offsetX;

	// Set the right bounds of the entities
	entityARect->w = entityARect->x +width - 1 -offsetW;
	entityBRect->w = entityBRect->x + entityB->width - 1 -entityB->offsetW;

	// Set the top bounds of the entities
	entityARect->y = (int)*stepY + offsetY;
	entityBRect->y = (int)entityB->y + entityB->offsetY;

	// Set the bottom bounds of the entities
	entityARect->h = entityARect->y + height - 1 - offsetH;
	entityBRect->h = entityBRect->y + entityB->height - 1 - entityB->offsetH;

	// Finally, check if collision is not happening
	if(entityARect->h < entityBRect->y)
	{
		return false; // If the bottom of the 1st is higher than the 2nd's top
	}
	if(entityARect->y > entityBRect->h) 
	{
		return false; // If the top of the 1st is lower than the 2nd's bottom
	}
	if(entityARect->w < entityBRect->x) 
	{	
		return false; // If the right of the 1st is smaller than the 2nd's left
	}
	if(entityARect->x > entityBRect->w)
	{
		return false; // If the left of blablabla....
	}

	// If you are here, then collision happened
	return true;
}

// TODO: OPTIMIZE


bool HB_Entity::pos_valid_tile(HB_Tile *tile)
{
	// If this tile does not have collisions
	if(tile == NULL)
	{
		return true;
	}

	// If this tile does not allow entities to go on it
	if(tile->typeID == TILE_BLOCK)
	{
		return false;
	}

	// In all the other cases it's ok, calm down
	return true;
}

bool HB_Entity::pos_valid_entity(HB_Entity *entityA, HB_Entity *entityB, float *stepX, float *stepY, int axis)
{
	if(entityA != entityB)
	{
		// A huge IF checking if the entities are ok and if the collision happened
		if(entityA != NULL && entityB != NULL && entityB->dead == false && entityA->dead == false &&
			entityB->flags ^ ENTITY_FLAG_MAPONLY && entityA->flags ^ ENTITY_FLAG_MAPONLY &&
			collision_check(entityB, stepX, stepY) == true)
		{
			// Create a new collision object and load the collision objects with the entities that have collided
			HB_EntityCollisions *entityColl = new HB_EntityCollisions(entityA, entityB, moveX, moveY, axis);

			// Push the collision event into the collisions events queue
			HB_EntityCollisions::entityColList.push_back(entityColl);

			// Say that the entity cannot move there
			return false;
		}
	}

	// If the checks above were all negative, the entity can move
	return true;
}