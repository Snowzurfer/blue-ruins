#pragma once

#include "HB_Global.h"
#include "HB.h"
#include "HB_Timer.h"
#include "HB_Surface.h"
#include "HB_GameStatesManager.h"


/// The game application
class HB
{
	private:

		/// If the application is running or not	
		bool running;

		/// The screen surface
		SDL_Surface *screen;

		// The manager of the game states
		HB_GameStatesManager *statesManager;
		

		/// Keeps track of the total frames passed
		int totalFrames;

		

		/// The frame rate regulator
		HB_Timer *deltaTime;

		/// The event structure that will be used
		SDL_Event eventu;

		
	private:
	
		/// Initializes the variables, returns false if something went wrong
		bool init();

		/// Instantiate the objects contained into the game
		void create_objects();

		/// Handles the input events
		void events(SDL_Event *Event);

		/// Updates the game engine
		void update(double deltaTicks);

		/// Prints the updated game
		void draw();

		/// Frees up the surface and quits SDL
		void clean_up();

		/// Loads the files and returns false if there were any errors
		bool load_files();

	public:

		/// The constructor
		HB();

		/// The function that actually runs the game into the main
		int run();
};