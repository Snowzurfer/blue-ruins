#include "HB_Animation.h"


HB_Animation::HB_Animation(std::string name, int mFrames, int w, int h, float *eX, float *eY)
{
	// Set the default values for all the attributes of the class
	currentFrameCol = 0;
	currentFrameRow = 0;
	maxFrames = 0;
	frameInc = 1;
	this->name = name;
	frameRate = 100;

	oscillate = false;

	spriteSheet = NULL;

	entityY = eY;
	entityX = eX;

	// Set the amount of sprites contained into the sprite sheet
	maxFrames = mFrames;

	// Set the width and the height of each frame
	SPRITE_WIDTH = w;
	SPRITE_HEIGHT = h;

	// Start the timer
	timer.start();
}

HB_Animation::~HB_Animation()
{
	// Free the surface used to hold the sprite sheet
	SDL_FreeSurface(spriteSheet);
}

bool HB_Animation::load_image(std::string path)
{
	// Load the sprite sheet
	if((spriteSheet = HB_Surface::load_image(path)) == NULL)
	{
		return false;
	}

	return true;
}

void HB_Animation::update()
{
	// If the frame switch time has passed
	if(timer.get_ticks() > frameRate)
	{
		// Increase the frame counter of frameInc's value
		currentFrameRow += frameInc;

		// If the animation must oscillate
		if(oscillate)
		{
			// If the frame incrementer is positive, then if the animation
			// index is increasing positively (left to right, in a sort of way)
			if(frameInc > 0)
			{
				// If the animation reached its end
				if(currentFrameRow >= maxFrames)
				{
					// Invert the increaser, so that the animation will play
					// the opposite way
					frameInc = -frameInc;
				}
			}
			// If the frame incrementer is negative, then if the animation
			// index is increasing negatively (right to left, in a sort of way)
			else
			{
				// If the animation reached its end
				if(currentFrameRow <= 0)
				{
					// Invert the increaser, so that the animation will play
					// the opposite way
					frameInc = -frameInc;
				}
			}
		}
		// If the animation must play from the straight
		else
		{
			// If the animation reached its end
			if(currentFrameRow >= maxFrames)
			{
				// Reset the frame counter
				currentFrameRow = 0;
			}
		}

		// Reset the timer
		timer.start();
	}
}

void HB_Animation::draw(SDL_Surface *screen)
{
	SDL_Rect clip; // TODO: OPTIMIZE

	clip.x = currentFrameRow * SPRITE_WIDTH;
	clip.y = 0;
	clip.h = SPRITE_HEIGHT;
	clip.w = SPRITE_WIDTH;

	HB_Surface::draw_surface(screen, *entityX - HB_Camera::cameraControl.cameraBounds.x, *entityY - HB_Camera::cameraControl.cameraBounds.y, spriteSheet, &clip);
}

void HB_Animation::set_current_frame(int frame)
{
	// Check if the passed parameter is correct
	if((frame >= 0) && (frame <= maxFrames))
	{
		currentFrameRow = frame;
	}
}

void HB_Animation::set_frame_rate(int rate)
{
	frameRate = rate;
}

int HB_Animation::get_current_frame()
{
	return currentFrameRow;
}

