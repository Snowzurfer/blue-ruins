#pragma once

#include "HB_Global.h"
#include "HB_Timer.h"
#include "HB_Surface.h"
#include "HB_Camera.h"

// LOL

class HB_Animation
{
	private:

		/// The surface used to draw the sprite
		SDL_Surface *spriteSheet;

		/// The width of each frame
		int SPRITE_WIDTH;
		/// The height of each frame
		int SPRITE_HEIGHT;
		/// Which direction the entity is facing to
		int *SPRITE_FACING;

		// Pointers to the entity position
		float *entityX;
		float *entityY;

		/// Identify the current frame to be shown
		int currentFrameCol, currentFrameRow;

		/// How many frames the animation must be incremented each time
		int frameInc;
		/// Time (in millisecond) that each frames is shown
		int frameRate;

		/// Time (in millisecond) of the last frame update
		HB_Timer timer;

		
		
	public:
		
		/// Wheter or not to repeat the sprite sheet sequence
		/// once it has come to the end (maxFrames).
		/// Thus the sequence would be: 1-2-3-2-1 when true
		bool oscillate;
		
		/// The number of frames of the current sprite sheet
		int maxFrames;

		///<summary>
		/// The name of the animation
		///</summary>
		std::string name;
	
	public:
	
		/// The constructor
		HB_Animation(std::string name, int mFrames, int w, int h, float *eX, float *eY);

		/// The deconstructor
		~HB_Animation();

		/// Load the sprite sheet used by the animation
		bool load_image(std::string path);

		/// Update the animation
		void update();
		
		/// Draw the updated animation
		void draw(SDL_Surface *screen);	

	public:

		/// Set the frame rate
		void set_frame_rate(int rate);

		/// Set the frame shown at the moment
		void set_current_frame(int frame);

		/// Get the frame currently shown
		int get_current_frame();
};