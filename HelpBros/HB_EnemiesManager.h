#pragma once

#include "HB_Global.h"
#include "HB_Timer.h"
#include "HB_EnemyRed.h"
#include "HB_EnemyGreen.h"
#include "HB_EnemyBot.h"
#include "HB_EntitiesManager.h"
#include <stdio.h>
#include <ctime>

class HB_EnemiesManager
{
	public:

		// Constructor
		HB_EnemiesManager(HB_Map *mapReference);

		// Deconstructor
		~HB_EnemiesManager(void);

		// Update the manager
		void update();

	private:

		// Timer defining enemies spawn time
		HB_Timer *spawnTimer;

		// Spawn time for the enemies
		double enemiesSpawnTime;

		// Pointer to the current map
		HB_Map *mapReference;

		// Which enemy the manager should spawn (defined randomly)
		int whichSpawn;
};

