#include "HB_EntitiesManager.h"



HB_EntitiesManager::HB_EntitiesManager()
{
	playerDead = false;

	enemiesDead = 0;
}

HB_EntitiesManager::~HB_EntitiesManager()
{
	// Clear everything
	HB_Entity::EntitiesPool.clear();
	HB_EntityCollisions::entityColList.clear();
}

void HB_EntitiesManager::update(double *deltaTicks)
{
	// Update each entity
	for(int i = 0; i < HB_Entity::EntitiesPool.size(); i++)
	{
		// If the entity is dead
		if(HB_Entity::EntitiesPool[i]->dead == true)
		{
			// If the entity to have died is the player
			if(HB_Entity::EntitiesPool[i]->type == ENTITY_TYPE_PLAYER)
			{
				playerDead = true;
			}
			// If the entity to have died is an enemy
			else if(HB_Entity::EntitiesPool[i]->type == ENTITY_TYPE_ENEMY)
			{
				enemiesDead ++;
			}
			
			// Remove it from the pool
			delete HB_Entity::EntitiesPool[i];
			HB_Entity::EntitiesPool.erase(HB_Entity::EntitiesPool.begin() + i);
		}
		else
		{
			// Update the pool
			HB_Entity::EntitiesPool[i]->update(deltaTicks);
		}
	}

	// After having updated the entities, check for their collisions

	// Entity vs Map
	for(int i = 0; i < HB_MapCollisions::mapColPoll.size(); i ++)
	{
		// Create two support pointers
		HB_Entity *supEntityA = HB_MapCollisions::mapColPoll[i]->entity;
		HB_Tile *supTile = HB_MapCollisions::mapColPoll[i]->tile;
		float *moveX = &HB_MapCollisions::mapColPoll[i]->moveX;
		float *moveY = &HB_MapCollisions::mapColPoll[i]->moveY;
		int *axis = &HB_MapCollisions::mapColPoll[i]->axis;

		// If the entity being checked still exists
		if(supEntityA != NULL && !supEntityA->dead)
		{
			// Tell entityA about its colliding with B
			if(supEntityA->on_collision_map(moveX, moveY, supTile, axis))
			{
				
			}
		}

		delete HB_MapCollisions::mapColPoll[i];
	}
	HB_MapCollisions::mapColPoll.clear();

	// Entitiy vs Entity
	for(int i = 0; i < HB_EntityCollisions::entityColList.size(); i ++)
	{
		// Create two support pointers
		HB_Entity *supEntityA = HB_EntityCollisions::entityColList[i]->entityA;
		HB_Entity *supEntityB = HB_EntityCollisions::entityColList[i]->entityB;
		float *moveX = &HB_EntityCollisions::entityColList[i]->moveX;
		float *moveY = &HB_EntityCollisions::entityColList[i]->moveY;
		int *axis = &HB_EntityCollisions::entityColList[i]->axis;

		// If the two entities being checked still exist
		if((supEntityA != NULL && supEntityB != NULL) && 
			(supEntityA->dead == false && supEntityB->dead == false) /*&&
			(supEntityA->state != ENTITY_STATE_BOLTED && supEntityB->state != ENTITY_STATE_BOLTED)*/)
		{
			// Tell entityA about its colliding with B
			if(supEntityA->on_collision_entity(supEntityB, moveX, moveY, axis))
			{
				// Tell B about collision only if necessary
				supEntityB->on_collision_entity(supEntityA, moveX, moveY, axis);
			}
		}

		delete HB_EntityCollisions::entityColList[i];
	}

	HB_EntityCollisions::entityColList.clear();
}

void HB_EntitiesManager::draw(SDL_Surface *destination)
{
	// Draw each entity
	for(int i = 0; i < HB_Entity::EntitiesPool.size(); i++)
	{
		HB_Entity::EntitiesPool[i]->draw(destination);
	}
}

void HB_EntitiesManager::handle_events(SDL_Event *events)
{
	// Handle events for each entity
	for(int i = 0; i < HB_Entity::EntitiesPool.size(); i++)
	{
		HB_Entity::EntitiesPool[i]->handle_events(events);
	}
}



void HB_EntitiesManager::add_entity(HB_Entity *entity)
{
	HB_Entity::EntitiesPool.push_back(entity);
}

