#pragma once

#include "HB_Global.h"
#include "HB_Surface.h"
#include "HB_Font.h"


class HB_HUD
{
	public:

		// Constructor
		HB_HUD(int *score);

		// Deconstructor
		~HB_HUD(void);
		
		/// Draw its contents
		void draw(SDL_Surface *screen);	

		bool load_files();

	private:
		
		HB_Font *font;

		SDL_Surface *srfc;

		// Current score
		int *score;

		// A string containing what to print
		char string[20];
		
			

};

