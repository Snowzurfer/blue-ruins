#pragma once

// The attributes of the screen
#define SCREEN_WIDTH 832
#define SCREEN_HEIGHT 832
#define SCREEN_BPP 32

// The pixels per second
#define PIXEL_PER_SECOND 35

// Size of a tile
#define TILE_SIZE 32

// Libraries to be included in each class
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <SDL_image.h>
#include <string>

static enum directions
{
	DIRECTION_RIGHT = 0,
	DIRECTION_LEFT,
	DIRECTION_UP,
	DIRECTION_DOWN
};

static enum axis
{
	AXIS_X = 0,
	AXIS_Y
};