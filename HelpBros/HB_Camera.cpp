#include "HB_Camera.h"

HB_Camera HB_Camera::cameraControl;

HB_Camera::HB_Camera()
{
	// Set the initial position of the camera
	cameraBounds.x = 0;
	cameraBounds.y = 0;
	cameraBounds.h = SCREEN_HEIGHT;
	cameraBounds.w = SCREEN_WIDTH;

	targetX = targetY = NULL;

	levelHeight = levelWidth = 0;
}

HB_Camera::~HB_Camera()
{
}

void HB_Camera::move(int moveX, int moveY)
{
	cameraBounds.x += moveX;
	cameraBounds.y += moveY;
}

void HB_Camera::set_position(int X, int Y)
{
	cameraBounds.x = X;
	cameraBounds.y = Y;
}

void HB_Camera::set_target(float *X, float *Y)
{
	targetX = X;
	targetY = Y;
}

void HB_Camera::set_levelBounds(int width, int height)
{
	levelWidth = width;
	levelHeight = height;
}

void HB_Camera::update()
{
	cameraBounds.x = (*targetX + 64 / 2) - SCREEN_WIDTH / 2;
	cameraBounds.y = (*targetY + 64 / 2) - SCREEN_HEIGHT / 2;

	if(cameraBounds.x < 0)
	{
		cameraBounds.x = 0;
	}
	if(cameraBounds.y < 0)
	{
		cameraBounds.y = 0;
	}
	if(cameraBounds.x + cameraBounds.w > levelWidth)
	{
		cameraBounds.x = levelWidth - cameraBounds.w;
	}
	if(cameraBounds.y + cameraBounds.h > levelHeight)
	{
		cameraBounds.y = levelHeight - cameraBounds.h;
	}
}