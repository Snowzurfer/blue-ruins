#include "HB_HUD.h"


HB_HUD::HB_HUD(int *score)
{
	this->score = score;

	font = new HB_Font(80, 255, 255, 255);
}


HB_HUD::~HB_HUD(void)
{
	delete font;
}

void HB_HUD::draw (SDL_Surface *dest)
{
	sprintf(string, "%s%d","", *score);
	font->draw(10, 10, string, srfc);

	HB_Surface::draw_surface(dest, 400, 50, srfc);

	SDL_Flip(srfc);
}

bool HB_HUD::load_files()
{
	// HUD transparency
	srfc = SDL_CreateRGBSurface(SDL_SWSURFACE, 200, 100, 32, 0, 0, 0, 0);
	Uint32 colorkey = SDL_MapRGB( srfc->format, 0, 0, 0 );
    SDL_SetColorKey( srfc, SDL_SRCCOLORKEY, colorkey );

	return true;
}
