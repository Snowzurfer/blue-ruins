#pragma once

// Libraries to include
#include "HB_Global.h"


// This class contains general manner functions, used wherever necessary in the code
class HB_Surface
{
	public:

		/// Load an optimized image
		static SDL_Surface *load_image( std::string filename );

		/// Apply a SDL_Surface to a destination Surface
		static void draw_surface(SDL_Surface *destination, int dX, int dY, SDL_Surface *source,
								  SDL_Rect* clipz = NULL);
};