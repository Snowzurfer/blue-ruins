#include "HB_GameOver.h"


HB_GameOver::HB_GameOver(void)
{
	background = NULL;
	message0 = NULL;
	menuMessage0 = NULL;
	menuMessage1 = NULL;
	menuMessage2 = NULL;
	nextStateID = STATE_NULL;
	font0 = TTF_OpenFont("./content/font/coders_crux.ttf", 140);
	font1 = TTF_OpenFont("./content/font/coders_crux.ttf", 72);
	textColor = new SDL_Color();
	textColor->b = textColor->g = textColor->r = 255;
	changeState = false;

	returnPressed = false;
}


HB_GameOver::~HB_GameOver(void)
{
	// Free surfaces
	TTF_CloseFont(font0);
	TTF_CloseFont(font1);
	SDL_FreeSurface(background);
	SDL_FreeSurface(message0);
	SDL_FreeSurface(menuMessage0);
	SDL_FreeSurface(menuMessage1);
	SDL_FreeSurface(menuMessage2);
	delete textColor;
}

void HB_GameOver::handle_events(SDL_Event *Event)
{
	// If a key was pressed
	if(Event->type == SDL_KEYDOWN)
	{
		// If r was pressed
		if(Event->key.keysym.sym == SDLK_r)
		{
			nextStateID = STATE_INGAME; // Start another game

			changeState = true;
		}
		// If the user wants to quit
		else if(Event->key.keysym.sym == SDLK_e)
		{
			nextStateID = STATE_EXIT; // Start another game

			changeState = true;
		}
		// If the user wants to go to the main menu
		else if(Event->key.keysym.sym == SDLK_m)
		{
			nextStateID = STATE_MENU; // Start another game

			changeState = true;
		}
	}
}

void HB_GameOver::update(double *deltaTicks)
{

}

bool HB_GameOver::load_files()
{
	// If there are problems with loading
	if((background = HB_Surface::load_image("./content/gfx/titlebg.png")) == NULL)
	{
		// Exit
		return false;
	}

	message0 = TTF_RenderText_Solid(font0, "GAME OVER!", *textColor);
	menuMessage0 = TTF_RenderText_Solid(font1, "(R)estart", *textColor);
	menuMessage2 = TTF_RenderText_Solid(font1, "(E)xit", *textColor);
	menuMessage1 = TTF_RenderText_Solid(font1, "(M)enu", *textColor);

	return true;
}

void HB_GameOver::draw(SDL_Surface *destination)
{
	HB_Surface::draw_surface(destination, (SCREEN_WIDTH - message0->w) / 2, 70, message0);
	HB_Surface::draw_surface(destination, (SCREEN_WIDTH - menuMessage0->w) / 2, 300, menuMessage0);
	HB_Surface::draw_surface(destination, (SCREEN_WIDTH - menuMessage1->w) / 2, 400, menuMessage1);
	HB_Surface::draw_surface(destination, (SCREEN_WIDTH - menuMessage2->w) / 2, 500, menuMessage2);
}

