#pragma once

#include "HB_Global.h"

class HB_Font
{
	public:

		// The font that will be used
		TTF_Font *font;

		SDL_Surface *textSurface;

		// The color of the font
		SDL_Color fontColor;
		SDL_Color backgroundColor;

		SDL_Rect pos;

		int Size;

	public:

		// The default constructor
		HB_Font(int fontSize, int R, int G, int B);

		void LoadFont(char *name);

		void draw(int x, int y, char *text, SDL_Surface *screen);

		~HB_Font();
};