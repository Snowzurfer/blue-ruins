#include "HB_Global.h"
#include "HB.h"

void HB::clean_up()
{
	// Delete entities
	/*for(int i = 0; i < HB_Entity::EntitiesList.size(); i++)
	{
		delete HB_Entity::EntitiesList[i];
	}
	HB_Entity::EntitiesList.clear();*/
	
	
	delete deltaTime;

	delete statesManager;

	// Free the used surfaces
	SDL_FreeSurface(screen);


	// Quit SDL_mixer
	Mix_CloseAudio();

	// Quit SDL_ttf
	TTF_Quit();

	//console.clean_up();

    // Quit SDL
    SDL_Quit();

}