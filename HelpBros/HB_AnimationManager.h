#pragma once

#include "HB_Global.h"
#include "HB_Animation.h"
#include <vector>

class HB_AnimationManager
{
	public:

		// Constructor
		HB_AnimationManager();

		// Deconstructor
		~HB_AnimationManager();

		/// Update its contents
		void update();
		
		/// Draw its contents
		void draw(SDL_Surface *screen);		
		
		///<summary>
		/// Add an animation to the pool
		///</summary>
		void add_animation(std::string name, int numFrames, int w, int h, float *entityX, 
							float *entityY);

		///<summary>
		/// Play a certain animation, return false if it does not exist
		///</summary>
		bool play_animation(std::string name);
		
		///<summary>
		/// Load the surface for that animation
		///</summary>
		bool load_image(std::string path, std::string animName);

	private:

		///<summary>
		/// The pool containing the animations
		///</summary>
		std::vector<HB_Animation*> animationsPool;

		///<summary>
		/// Index of the animation to be played at the moment
		///</summary>
		int current_animation;
};