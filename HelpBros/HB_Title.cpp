#include "HB_Title.h"


HB_Title::HB_Title(void)
{
	background = NULL;
	message0 = NULL;
	menuMessage0 = NULL;
	nextStateID = STATE_MENU;
	font0 = TTF_OpenFont("./content/font/coders_crux.ttf", 72);
	font1 = TTF_OpenFont("./content/font/coders_crux.ttf", 52);
	textColor = new SDL_Color();
	textColor->b = textColor->g = textColor->r = 255;
	changeState = false;

	returnPressed = false;
}


HB_Title::~HB_Title(void)
{
	// Free surfaces
	TTF_CloseFont(font0);
	TTF_CloseFont(font1);
	SDL_FreeSurface(background);
	SDL_FreeSurface(message0);
	SDL_FreeSurface(menuMessage0);
	delete textColor;
}

void HB_Title::handle_events(SDL_Event *Event)
{
	// If a key was pressed
	if(Event->type == SDL_KEYDOWN)
	{
		// If return was pressed
		if(Event->key.keysym.sym == SDLK_RETURN)
		{
			returnPressed = true;
		}
	}
	// If the key was relased
	if(Event->type == SDL_KEYUP)
	{
		// If return was pressed
		if(Event->key.keysym.sym == SDLK_RETURN)
		{
			if(returnPressed)
			{
				returnPressed = false;

				// Change state
				changeState = true;
			}
		}
	}
}

void HB_Title::update(double *deltaTicks)
{

}

bool HB_Title::load_files()
{
	// If there are problems with loading
	if((background = HB_Surface::load_image("./content/gfx/titlebg.png")) == NULL)
	{
		// Exit
		return false;
	}

	message0 = TTF_RenderText_Solid(font0, "BLUE_RUINS", *textColor);
	menuMessage0 = TTF_RenderText_Solid(font1, "Press <ENTER>", *textColor);

	return true;
}

void HB_Title::draw(SDL_Surface *destination)
{
	//HB_Surface::draw_surface(destination, 0, 0, background);
	
	HB_Surface::draw_surface(destination, (SCREEN_WIDTH - message0->w) / 2, (SCREEN_HEIGHT - message0->h) / 2, message0);
	HB_Surface::draw_surface(destination, (SCREEN_WIDTH - menuMessage0->w) / 2, ((SCREEN_HEIGHT - menuMessage0->h) / 2) + 40, menuMessage0);
}

