#pragma once

#include "HB_Global.h"
#include "HB_Entity.h"
#include "HB_AnimationManager.h"


class HB_Bolt :
	public HB_Entity
{
	public:

		HB_Bolt(int posX, int posY, HB_Map *mapReference);

		~HB_Bolt(void);

		/// Update the entity
		void update(double *deltaTicks);

		/// Draw the entity onto a destination SDL_Surface
		void draw(SDL_Surface *Dest);

		// Happens when this entity has collided with another one
		bool on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis);

		// Happens when this entity has collided with the map
		bool on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *direction);

		void handle_events(SDL_Event *Event);

	private:

		HB_AnimationManager *animation;

};

