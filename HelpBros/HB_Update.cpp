#include "HB_Global.h"
#include "HB.h"

void HB::update(double deltaTicks)
{
	/*for(int i = 0; i < HB_Entity::EntitiesList.size(); i++)
	{
		HB_Entity::EntitiesList[i]->update(&deltaTicks);
	}*/

	statesManager->update(&deltaTicks);

	if(statesManager->currentStateID == STATE_EXIT)
	{
		// Exit the game
		running = false;
	}
}