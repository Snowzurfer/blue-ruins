#pragma once

#include "Timer.h"
#include <SDL.h>

// The main constructor
Timer::Timer()
{
	// Initialize the variables
	startTicks = 0;
	pausedTicks = 0;
	paused = false;
	started = false;
}

// Start the timer
void Timer::start()
{
	// Start the timer
	started = true;

	// Unpause the timer
	paused = false;

	// Get the current clock time
	startTicks = SDL_GetTicks();
}

// Stop the timer
void Timer::stop()
{
	// Stop the timer
	started = false;

	// Unpause the timer
	paused = false;
}

// Returns the current time, and 0 if the timer has not started yet
int Timer::get_ticks()
{
	// If the timer is running
	if(started == true)
	{
		// If the timer is paused
		if(paused == true)
			return pausedTicks; // Return the number of ticks when the timer was paused
		else
			return SDL_GetTicks() - startTicks; // Return the current time minus the start time
	}

	// If the timer is not running
	return 0; 
}

// Pause the timer
void Timer::pause()
{
	// If the timer is running and it is not already paused
	if((started == true) && (paused == false))
	{
		// Pause the timer
		paused = true;

		// Calculate the passed ticks
		pausedTicks = SDL_GetTicks() - startTicks;
	}
}

// Unpause the timer
void Timer::unpause()
{
	// If the timer is paused
	if(paused == true)
	{
		// Unpause the timer
		paused = false;

		// Reset the starting ticks
		startTicks = SDL_GetTicks() - pausedTicks;

		// Reset the paused timer
		pausedTicks = 0;
	}
}

bool Timer::is_paused()
{
	return paused;
}

bool Timer::is_started()
{
	return started;
}