#include "HB_Console.h"


HB_Console::HB_Console(HB_Timer *fpsTimer,  float *plx, float *ply, float *velx, float *vely)
{	
	/// Link the pointer to the game HB_Timer
	fps = fpsTimer;
	// frames = totalFPS;

	font = new HB_Font(24, 255, 255, 255);

	HB_Console::plx = plx;
	HB_Console::ply = ply;
	HB_Console::velx = velx;
	HB_Console::vely = vely;
	
	cont = 0;

	second = 1000;

	lastfps = 0;

	
}

HB_Console::~HB_Console()
{
	// Deallocate pointers
	delete font;
}

void HB_Console::update(double *deltaTicks)
{
	cont++;

	if(cont >= 50)
	{
		cont = 0;
		lastfps = second/(*deltaTicks);
	}
}

void HB_Console::draw(SDL_Surface *screen)
{
	

	int playerx = (int)*plx;
	char string[20];
	// sprintf(string, "%d", playerx);
	sprintf(string,"%s%d","X= ",playerx);
	font->draw(10,10,string,srfc);

	int playery = (int)*ply;
	sprintf(string,"%s%d","Y= ",playery);
	font->draw(10,25,string,srfc);

	int playervelx = (int)*velx;
	sprintf(string,"%s%d","VelX= ",playervelx);
	font->draw(10,40,string,srfc);

	int playervely = (int)*vely;
	sprintf(string,"%s%d","VelY= ",playervely);
	font->draw(10,55,string,srfc);
	
	int gfps = (float)lastfps;
	sprintf(string,"%s%d","Fps= ",gfps);
	font->draw(10,70,string,srfc);
	
	SDL_BlitSurface(srfc, NULL, screen,NULL);

	SDL_Flip(srfc);
}

bool HB_Console::load_files()
{
	// Console transparency
	srfc = SDL_CreateRGBSurface(SDL_SWSURFACE, 200, 100, 32, 0,0, 0, 0);
	Uint32 colorkey = SDL_MapRGB( srfc->format, 0, 0, 0 );
    SDL_SetColorKey( srfc, SDL_SRCCOLORKEY, colorkey );

	return true;
}