#include "HB_Tile.h"

HB_Tile::HB_Tile(int x, int y, int type, int ID, SDL_Surface *tileSheet)
{
	tileID = ID;
	typeID = type;
	box = new SDL_Rect();
	box->x = x;
	box->y = y;
	box->w = x + TILE_SIZE;
	box->h = y + TILE_SIZE;
	this->tileSheet = tileSheet;
}

void HB_Tile::draw(SDL_Surface *destination, SDL_Rect *clip)
{
	// Check collision between the tile and the camera
	if(collisionWithCamera(&HB_Camera::cameraControl.cameraBounds) == true)
	{
		// Draw the tile relating to the camera's position
		HB_Surface::draw_surface(destination, box->x - HB_Camera::cameraControl.cameraBounds.x, box->y - HB_Camera::cameraControl.cameraBounds.y, tileSheet, clip);
	}
}

bool HB_Tile::collisionWithCamera(SDL_Rect *cameraBox)
{
	// If the right bound of the tile is out of the camera's bounds
	if(box->w < cameraBox->x) return false;
	// If the top bound of the tile is out of the camera's bounds
	if(box->y > cameraBox->y + cameraBox->h) return false;
	// If the left bound of the tile is out of the camera's bounds
	if(box->x > cameraBox->x + cameraBox->w) return false;
	// If the bottom bound of the tile is out of the camera's bounds
	if(box->h < cameraBox->y) return false;

	return true;

}