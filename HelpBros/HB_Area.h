#pragma once

#include "HB_Global.h"
#include "HB_Map.h"

class HB_Area
{
	public:

		/// Static controller for the areas
		//static HB_Area areaControl;
		
	public:

		/// Pool for the map objects
		std::vector<HB_Map> mapList;

	private:

		/// Size of the area in maps
		int areaSize;


		SDL_Surface *surfaceTileset;

	public:

		/// Default constructor
		HB_Area();

		/// Default deconstructor
		~HB_Area();

		/// Load the files
		bool load_files(char *file);

		/// Draw the area
		void draw(SDL_Surface *destination, int cameraX, int cameraY);

		/// Clean up the area
		void on_cleanup();

		HB_Map *get_map(int x, int y, int mapW, int mapH);
		HB_Tile *get_tile(int x, int y);
};