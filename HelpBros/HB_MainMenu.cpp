#include "HB_MainMenu.h"


HB_MainMenu::HB_MainMenu(void)
{
	background = NULL;
	message0 = NULL;
	for(int i=0; i<4; i++)
	{
		buttonHighlight[i] = NULL;
		buttonUnused[i] = NULL;
	}
	nextStateID = STATE_NULL;
	font0 = TTF_OpenFont("./content/font/coders_crux.ttf", 120);
	font1 = TTF_OpenFont("./content/font/coders_crux.ttf", 72);
	font2 = TTF_OpenFont("./content/font/coders_crux.ttf", 72);
	textColor = new SDL_Color();
	white = new SDL_Color();
	blue = new SDL_Color();
	textColor->b = textColor->g = textColor->r = 255;
	white->b = white->g = white->r = 255;
	blue->b = 255;
	blue->g = blue->r = 0;
	changeState = false;
	selection = MENU_START;
	selectionIndex = 0;

	upPressed = downPressed = returnPressed = false;
}


HB_MainMenu::~HB_MainMenu(void)
{
	// Free surfaces
	TTF_CloseFont(font0);
	TTF_CloseFont(font1);
	TTF_CloseFont(font2);
	SDL_FreeSurface(background);
	SDL_FreeSurface(message0);
	for (int i = 0; i < 4; i++)
	{
		SDL_FreeSurface(buttonHighlight[i]);
		SDL_FreeSurface(buttonUnused[i]);
	}
	//SDL_FreeSurface(message2);
	delete textColor;
	delete white;
	delete blue;
	
}

void HB_MainMenu::handle_events(SDL_Event *Event)
{
	// If a key was pressed
	if(Event->type == SDL_KEYDOWN)
	{
		// If escape was pressed
		if(Event->key.keysym.sym == SDLK_ESCAPE)
		{
			// Set the index on the exit button
			selection = MENU_EXIT;
		}
		// If return was pressed
		else if(Event->key.keysym.sym == SDLK_RETURN)
		{
			// Check which button was selected
			switch (selection)
			{
				// Start a new game
				case MENU_START:
				{
					nextStateID = STATE_INGAME;
					changeState = true;
					break;
				}
				// Exit the game
				case MENU_EXIT:
				{
					nextStateID = STATE_EXIT;
					changeState = true;
					break;
				}
				case MENU_CREDITS:
				{
					nextStateID = STATE_CREDITS;
					changeState = true;
					break;
				}
				case MENU_OPTIONS:
				{
					nextStateID = STATE_SETTINGS;
					changeState = true;
					break;
				}
			}
		}
		// If up was pressed
		else if(Event->key.keysym.sym == SDLK_UP)
		{
			// If the button isn't already pressed
			if(!upPressed)
			{
				selectionIndex --;

				// If the index has gone too far up
				if(selectionIndex < 0)
				{
					// Reset it
					selectionIndex = 3;
				}

				upPressed = true;
			}

			// Color the selectioned button
		//	set_button_color(selectionIndex);
		}
		else if(Event->key.keysym.sym == SDLK_DOWN)
		{
			// If the button isn't already pressed
			if(!downPressed)
			{
				selectionIndex ++;

				// If the index has gone too far down
				if(selectionIndex > 3)
				{
					// Reset it
					selectionIndex = 0;
				}

				downPressed = true;
			}
			// Color the selectioned button
			//set_button_color(selectionIndex);
		}
	}
	// If a key was released
	if(Event->type == SDL_KEYUP)
	{
		if(Event->key.keysym.sym == SDLK_UP)
		{
			upPressed = false;
		}
		else if(Event->key.keysym.sym == SDLK_DOWN)
		{
			downPressed = false;
		}
	}
}

void HB_MainMenu::update(double *deltaTicks)
{
	// Set the selection
	switch (selectionIndex)
	{
		case 0:
			selection = MENU_START;
			break;
		case 1:
			selection = MENU_OPTIONS;
			break;
		case 2:
			selection = MENU_CREDITS;
			break;
		case 3:
			selection = MENU_EXIT;
			break;
	}

}

void HB_MainMenu::draw(SDL_Surface *destination)
{

	HB_Surface::draw_surface(destination, (SCREEN_WIDTH - message0->w) / 2, 50, message0);

	for(int i = 0; i< 4; i++)
	{
		if(selectionIndex == i)
		{
			HB_Surface::draw_surface(destination, (SCREEN_WIDTH - buttonHighlight[i]->w) / 2, (i * 100) + 300, buttonHighlight[i]);
		}
		else
		{
			HB_Surface::draw_surface(destination, (SCREEN_WIDTH - buttonUnused[i]->w) / 2, (i * 100) + 300, buttonUnused[i]);
		}
	}
}

bool HB_MainMenu::load_files()
{
	// If there are problems with loading
	if((background = HB_Surface::load_image("./content/gfx/titlebg.png")) == NULL)
	{
		// Exit
		return false;
	}

	message0 = TTF_RenderText_Solid(font0, "BLUE_RUINS", *textColor);
	buttonUnused[0] = TTF_RenderText_Solid(font1, "Start Game", *white);
	buttonUnused[1] = TTF_RenderText_Solid(font1, "Settings", *white);
	buttonUnused[2] = TTF_RenderText_Solid(font1, "Credits", *white);
	buttonUnused[3] = TTF_RenderText_Solid(font1, "Quit", *white);

	buttonHighlight[0] = TTF_RenderText_Solid(font1, "Start Game", *blue);
	buttonHighlight[1] = TTF_RenderText_Solid(font1, "Settings", *blue);
	buttonHighlight[2] = TTF_RenderText_Solid(font1, "Credits", *blue);
	buttonHighlight[3] = TTF_RenderText_Solid(font1, "Quit", *blue);

	return true;
}
