#pragma once

#include "HB_Global.h"
#include "HB_Surface.h"

class HB_Camera
{
	private:
		
		/// Position of the camera's target
		float *targetX, *targetY;
		
		//
		int  levelWidth, levelHeight;


	public:

		static HB_Camera cameraControl;

		/// Default constructor
		HB_Camera();

		~HB_Camera();

		/// Move the camera
		void move(int moveX, int moveY);

		/// <summary>
		/// Updates the camera position
		/// </summary>
		void update();

		/// Set the coordinates of the camera
		void set_position(int X, int Y);

		/// Set the target of the camera
		void set_target(float *X, float *Y);

		/// <summary>
		/// Set the level bounds for the camera
		/// </summary>
		void set_levelBounds(int width, int height);

		/// The camera
		SDL_Rect cameraBounds;
};