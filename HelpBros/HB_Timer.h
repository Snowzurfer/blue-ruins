#pragma once

/// The timer class
class HB_Timer
{
	private:
		
		/// The clock when the timer started
		double startTicks;

		/// The ticks stored when the timer paused
		double pausedTicks;

		// The timer status
		bool paused;
		bool started;

	public:

		/// The constructor
		HB_Timer();

		// The various clock actions
		void start();
		void stop();
		void pause();
		void unpause();

		/// Get the timer's time
		double get_ticks();

		// Check the status of the timer
		bool is_paused();
		bool is_started();
};