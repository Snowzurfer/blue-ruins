#pragma once

#include "HB_Global.h"
#include "HB_GameState.h"
#include "HB_InGame.h"
#include "HB_Title.h"
#include "HB_MainMenu.h"
#include "HB_GameOver.h"


class HB_GameStatesManager
{
	public:

		// Constructor
		HB_GameStatesManager(HB_Timer *deltaTime);

		// Deconstructor
		~HB_GameStatesManager(void);

		/// Handles the input events
		void events(SDL_Event *Event);

		/// Updates the game engine
		void update(double *deltaTicks);

		/// Prints the updated game
		void draw(SDL_Surface *destination);

		/// Loads the files and returns false if there were any errors
		bool load_files();

		// Set the upcoming state
		void set_next_state(int newState);

		// Change the state
		void change_state();

		int currentStateID;

	private:

		// State variables
		
		int nextStateID;

		// The current state
		HB_GameState *currentState;

		HB_Timer *deltaTime;
};

