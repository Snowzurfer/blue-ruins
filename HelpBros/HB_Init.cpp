#include "HB.h"
#include <iostream>
using namespace std;

/// Initializes the variables, returns false if something went wrong
bool HB::init()
{
	// Initialize all SDL subsystems
	if( SDL_Init( SDL_INIT_EVERYTHING ) == -1)
	{
		return false;
	}

	SDL_EnableKeyRepeat(1, SDL_DEFAULT_REPEAT_INTERVAL);
    
	// Set up the screen
	if((screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL)
	{
		return false;
	}

	// Set the window caption
	SDL_WM_SetCaption( "Blue Ruins", NULL);

	// Initialize SDL_ttf
	if(TTF_Init() == -1)
	{
		return false;
	}

	// Initialize SDL_mixer
	if(Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		return false;
	}

	// Initialize the console
	//console.init(&fps, &totalFrames, screen);

	// If everything initialized fine
	return true;
}