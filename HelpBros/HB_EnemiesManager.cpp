#include "HB_EnemiesManager.h"


HB_EnemiesManager::HB_EnemiesManager(HB_Map *mapReference)
{
	spawnTimer = new HB_Timer();

	spawnTimer->start();

	enemiesSpawnTime = 3000; // 2 seconds

	this->mapReference = mapReference;

	// Seed the random numbers generator
	srand((unsigned)time(0));
}


HB_EnemiesManager::~HB_EnemiesManager(void)
{
	delete spawnTimer;
}

void HB_EnemiesManager::update()
{
	// If it's time to spawn an enemy
	if(spawnTimer->get_ticks() >= enemiesSpawnTime)
	{
		whichSpawn = rand() % 3;

		if(whichSpawn == 0)
		{
			// Add a red enemy to the pool
			HB_Entity::EntitiesPool.push_back(new HB_EnemyRed(415, 30, mapReference));
		}
		else if (whichSpawn == 1)
		{
			// Add a green enemy to the pool
			HB_Entity::EntitiesPool.push_back(new HB_EnemyGreen(415, 30, mapReference));
		}
		else
		{
			// Add a bot enemy to the pool
			HB_Entity::EntitiesPool.push_back(new HB_EnemyBot(415, 30, mapReference));
		}

		// Reset the timer
		spawnTimer->start();

		// Increase difficulty
		enemiesSpawnTime -= 0.2;
	}
}