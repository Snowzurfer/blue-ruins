#include "HB.h"

void HB::events(SDL_Event *Event)
{
	// If the user closed the window
	if(Event->type == SDL_QUIT || (Event->type == SDL_KEYDOWN && Event->key.keysym.sym == SDLK_ESCAPE))
	{
		// Quit the program
		running = false;
	}

	// Handle events for the current state
	statesManager->events(Event);
}