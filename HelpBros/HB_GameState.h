#pragma once

#include "HB_Global.h"
#include "HB_Surface.h"
#include "HB_Font.h"

enum gameStates
{
	STATE_NULL = 0,
	STATE_INTRO,
	STATE_TITLE,
	STATE_INGAME,
	STATE_GAMEOVER,
	STATE_MENU,
	STATE_EXIT,
	STATE_CREDITS,
	STATE_SETTINGS
};

class HB_GameState
{
	public:

		// Contructor
		HB_GameState(void);

		// Deconstructor
		virtual ~HB_GameState(void) {};

		virtual void handle_events(SDL_Event *Event) = 0;

		virtual void update(double *deltaTicks) = 0;

		virtual void draw(SDL_Surface *destination) = 0;

		virtual bool load_files() = 0;

		// The ID of the next state
		int nextStateID;

		// Wheter the manager should change state or not
		bool changeState;
};

