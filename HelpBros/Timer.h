#pragma once

/// The timer class
class Timer
{
	private:
		
		/// The clock when the timer started
		int startTicks;

		/// The ticks stored when the timer paused
		int pausedTicks;

		// The timer status
		bool paused;
		bool started;

	public:

		/// The constructor
		Timer();

		// The various clock actions
		void start();
		void stop();
		void pause();
		void unpause();

		/// Get the timer's time
		int get_ticks();

		// Check the status of the timer
		bool is_paused();
		bool is_started();
};