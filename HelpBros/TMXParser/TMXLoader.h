#ifndef TMXLOADER_H
#define TMXLOADER_H


#include <SDL.h>
#include <SDL_image.h>


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;


#include "base64.h"
#include "tinyxml/tinyxml.h"

class TMXLoader : public TiXmlVisitor
{
    public:
        TMXLoader();
        virtual ~TMXLoader();


        void cleanup();

		virtual bool 	VisitEnter  (const TiXmlDocument  &);

        virtual bool 	VisitExit (const TiXmlDocument &);

        virtual bool 	VisitEnter (const TiXmlElement &, const TiXmlAttribute *);

        virtual bool 	VisitExit (const TiXmlElement &);

        virtual bool 	Visit (const TiXmlDeclaration &);

        virtual bool 	Visit (const TiXmlText &);

        virtual bool 	Visit (const TiXmlComment &);

        virtual bool 	Visit (const TiXmlUnknown &);

        bool loadDocument(std::string file);

        int getNumMapColumns() { return widthInTiles; }
        int getNumMapRows() { return heightInTiles; }
		string getBGPath() { return backgroundPath; }
		string getTilesetPath() { return tilesetPath; }

		vector< vector< int > > getBackgroundData() { return bg_LayerData; }
		vector< vector< int > > getTilesetData(){ return m_LayerData; }

    protected:

    private:

		// Height and width in tiles of the map
        int widthInTiles;
        int heightInTiles;

		int backgroundTilesWidth;
		int backgroundTilesHeight;

		int tilesetTilesWidth;
		int tilesetTilesHeight;

		// Hold the path to the files
		string backgroundPath;
		string tilesetPath;

		int actualTileset;

        vector< vector< int > > m_LayerData;
		vector< vector< int > > bg_LayerData;


        void decode_and_store_map_data( string encoded_data, vector< vector< int > > &m_LayerData);

};

#endif // TMXLOADER_H
