


#include "TMXLoader.h"

TMXLoader::TMXLoader()
{
	actualTileset = 0;

	backgroundTilesHeight = backgroundTilesWidth = tilesetTilesHeight = tilesetTilesWidth = 0;
}

TMXLoader::~TMXLoader()
{
}

bool 	TMXLoader::VisitEnter  (const TiXmlDocument  &doc)
{
    return true; //TODO: for performance, we may not want to return true for each of these callbacks for the visitor pattern.
}

bool 	TMXLoader::VisitExit (const TiXmlDocument &doc)
{
    return true;
}

bool 	TMXLoader::VisitEnter (const TiXmlElement &elem, const TiXmlAttribute *attrib)
{
    if (string("map") == elem.Value()) // Correct
	{
		elem.Attribute("width", &widthInTiles);
		elem.Attribute("height", &heightInTiles);

    }
    else if (string("tileset") == elem.Value()) 
	{
		// Read the ID of that tileset
		elem.Attribute("firstgid", &actualTileset);

		// Read the path to the tileset used for bulding that layer
		if(actualTileset == 1) // BG
		{
			backgroundPath = elem.FirstChildElement()->Attribute("source");
		}
		else if(actualTileset == 2) // Tiles
		{
			tilesetPath = elem.FirstChildElement()->Attribute("source");
		}
    }
    else if (string("layer") == elem.Value()) 
	{
        
        if(elem.Attribute("name") == string("Background"))
		{
			elem.Attribute("width", &backgroundTilesWidth);
			elem.Attribute("height", &backgroundTilesHeight);

			const char* text = elem.FirstChildElement()->GetText();
			decode_and_store_map_data( text, bg_LayerData);
		}
		else if(elem.Attribute("name") == string("Tiles"))
		{
			elem.Attribute("width", &tilesetTilesWidth);
			elem.Attribute("height", &tilesetTilesHeight);

			const char* text = elem.FirstChildElement()->GetText();
			decode_and_store_map_data( text, m_LayerData );
		}
	}

	return true;
}

 bool 	TMXLoader::VisitExit (const TiXmlElement &elem)
{
    return true;
}

 bool 	TMXLoader::Visit (const TiXmlDeclaration &dec)
{
    return true;
}

 bool 	TMXLoader::Visit (const TiXmlText &text)
{
    return true;
}

 bool 	TMXLoader::Visit (const TiXmlComment &comment)
{
    return true;
}

 bool 	TMXLoader::Visit (const TiXmlUnknown &unknown)
{
    return true;
}


bool TMXLoader::loadDocument(std::string file)
{
	TiXmlDocument doc(file.c_str());

    if ( ! doc.LoadFile() ) {
		return false;
	}

    //TiXmlElement* elem = doc.RootElement();

    doc.Accept(this);

    return true;
}

void TMXLoader::decode_and_store_map_data( string encoded_data, vector< vector< int > > &layerData)
{
	// Load a vector representing a row
	vector< int > layerDataRow( widthInTiles );
    int m_LayerRow = 0;
    int m_LayerCol = 0;

	// Decode the data
    vector<int> unencoded_data = base64_decode(encoded_data);

    for (int i = 0; i < getNumMapRows(); i++)
    {
        layerData.push_back( layerDataRow );
    }

    for (int i = 0; i < unencoded_data.size(); i += 4)
    {
        // Get the grid ID
        int gid = unencoded_data[i] | unencoded_data[i + 1] << 8 | unencoded_data[i + 2] << 16 | unencoded_data[i + 3] << 24;

        layerData[m_LayerRow][m_LayerCol] = gid;

		if ((i + 4) % ( widthInTiles * 4) == 0) 
		{
            m_LayerRow++;
            m_LayerCol = 0;
        }
        else 
		{
            m_LayerCol++;
        }
    }

//    for (int row = 0; row < NUM_LAYER_ROWS; row++)
//    {
//        for (int col = 0; col < NUM_LAYER_COLS; col++)
//        {
//           myfile << " (" << layerData[row][col] << ") ";
//        }
//
//        myfile << endl;
//    }
}
/*****************************************************/
