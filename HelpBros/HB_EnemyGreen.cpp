#include "HB_EnemyGreen.h"


HB_EnemyGreen::HB_EnemyGreen(int xPos, int yPos, HB_Map *mapReference)
	:HB_Entity(32,32, mapReference)
{

	animations = new HB_AnimationManager();

	x = xPos;
	y = yPos;

	animations->add_animation("MoveR", 4, 32, 32, &x, &y);
	animations->add_animation("MoveL", 4, 32, 32, &x, &y);
	animations->add_animation("Destroy", 6, 32, 32, &x, &y);
	animations->add_animation("Bolted", 2, 32, 32, &x, &y);
	
	// Set the range of speed
	rangeMin = 2.5;
	rangeMax = 4.5;
	// Seed the random numbers generator
	srand((unsigned)time(0));

	maxVelX = rangeMin + (rand() % (int)(rangeMax - rangeMin));
	maxVelY = 12;
	xAccFactor = 3;
	xDecFactor = 1.5f;

	fallingPlayed = dyingPlayed = moveLPlayed = moveRPlayed = boltedPlayed = false;

	type = ENTITY_TYPE_ENEMY;
	enemyType = ENEMY_GREEN;

	if((rand() % 2) == 0)
	{
		movingRight = true;
	}
	else
	{
		movingLeft = true;
	}

	HP = 800;

	load_files();

	dyingAnimTime = 500;

	boltedAnimTime = 500;

	setAnimBack = false;

	pixelMoved = false;
}


HB_EnemyGreen::~HB_EnemyGreen(void)
{
	// Delete animations
	delete animations;
}

void HB_EnemyGreen::draw(SDL_Surface *destination)
{
	HB_Entity::draw(destination);

	//movingAnim->draw(destination);

	animations->draw(destination);
}

bool HB_EnemyGreen::load_files()
{
	

	// Right animation sprite
	if(animations->load_image("./content/gfx/Green_move_fiveframes.png", "MoveR") == false)
	{
		return false;
	}
	// Left animation sprite
	if(animations->load_image("./content/gfx/Green_move_fiveframes.png", "MoveL") == false)
	{
		return false;
	}
	// Destroy sprite
	if(animations->load_image("./content/gfx/Green_destroy.png", "Destroy") == false)
	{
		return false;
	}
	// Bolted sprite
	if(animations->load_image("./content/gfx/Green_Bolted.png", "Bolted") == false)
	{
		return false;
	}

	return true;
}

void HB_EnemyGreen::update(double *deltaTicks)
{
	// Update the parent class
	HB_Entity::update(deltaTicks);

	if(setAnimBack == true)
	{
		animations->play_animation("MoveR");
		setAnimBack = false;
	}

	/* This enemy simply moves from right to the left,
		and when it collides with the bounds, it simply
		turns and start moving again */

	animations->update();
}

bool HB_EnemyGreen::on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis)
{

	HB_Entity::on_collision_entity(entity, moveX, moveY, axis);

	if(entity->type == ENTITY_TYPE_BULLET)
	{
		if(HP > 1)
		{
			hurt(1);
			flags = ENTITY_FLAG_MAPONLY;
			state = ENTITY_STATE_BOLTED;
			boltedAnimTimer->start();
			animations->play_animation("Bolted");
		}
		else if( HP <= 1)
		{
			flags = ENTITY_FLAG_MAPONLY;
			state = ENTITY_STATE_DYING;
			dyingAnimTimer->start();
			animations->play_animation("Destroy");
		}

		return true;
	}
	else if(entity->type == ENTITY_TYPE_PLAYER)
	{
		flags = ENTITY_FLAG_MAPONLY;
		state = ENTITY_STATE_DYING;
		dyingAnimTimer->start();
		animations->play_animation("Destroy");

		return true;
	}
	else if(entity->type == ENTITY_TYPE_BOLT)
	{
		if(HP > 1)
		{
			hurt(1);
			//flags = ENTITY_FLAG_MAPONLY;
			state = ENTITY_STATE_BOLTED;
			//boltedAnimTimer->start();
			if(!boltedPlayed)
			{
				boltedPlayed = true;
				animations->play_animation("Bolted");
			}

			// Move the entity for one pixel to the bolt
			if(!pixelMoved)
			{
				pixelMoved = true;

				// If the entity was moving right
				if(*moveX > 0)
				{
					x ++;
				}
				// If the entity was moving left
				else if(*moveX < 0)
				{
					x --;
				}
			}
		}
		else if( HP <= 1)
		{
			flags = ENTITY_FLAG_MAPONLY;
			state = ENTITY_STATE_DYING;
			dyingAnimTimer->start();
			animations->play_animation("Destroy");
		}

		return true;
	}

	/* This enemy simply moves from right to the left,
		and when it collides with the bounds, it simply
		turns and start moving again */
	// If the collision happened on the X axis
	if(*axis == AXIS_X)
	{
		// If the entity was moving right
		if(*moveX > 0)
		{
			movingLeft = true;
			movingRight = false;
			animations->play_animation("MoveL");
		}
		// If the entity was moving left
		if(*moveX < 0)
		{
			movingLeft = false;
			movingRight = true;
			collidedLR = 1;
			animations->play_animation("MoveR");
		}
	}

	return false;
}

bool HB_EnemyGreen::on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *axis)
{

	HB_Entity::on_collision_map(moveX, moveY, tile, axis);

	// If the collision happened on the X axis
	if(*axis == AXIS_X)
	{
		/* This enemy simply moves from right to the left,
		and when it collides with the bounds, it simply
		turns and start moving again */

		// If the entity was moving right
		if(*moveX > 0)
		{
			movingLeft = true;
			movingRight = false;
			animations->play_animation("MoveL");
		}
		// If the entity was moving left
		if(*moveX < 0)
		{
			movingLeft = false;
			movingRight = true;
			animations->play_animation("MoveR");
		}

		return false;
	}

	return false;
}

void HB_EnemyGreen::handle_events(SDL_Event *Event)
{

}
