#pragma once

#include "HB_Global.h"
#include "HB_Surface.h"
#include "HB_Camera.h"
#include <vector>

class HB_Background
{
	public:

		// Constructor
		HB_Background();

		// Deconstructor
		~HB_Background();

		// Load the image representing the background
		bool load_image(std::string filePath, std::vector< std::vector< int > > backgroundData, int w, int h);

		// Update the background
		void update();

		// Draw the background
		void draw(SDL_Surface *destination);

	private:

		SDL_Surface *backgroundSurface;

		SDL_Rect whatToDraw;
};