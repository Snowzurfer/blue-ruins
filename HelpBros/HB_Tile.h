#pragma once

#include "HB_Global.h"
#include "HB_Surface.h"
#include "HB_Camera.h"

enum // TODO: OPTIMIZE
	{
		TILE_NONE = 0,
		TILE_NORMAL,
		TILE_BLOCK
		
	};

class HB_Tile
{
	private:

		/// Coordinates of the tile
		SDL_Rect *box;

		/// Check wether the tile is must be shown or not
		bool collisionWithCamera(SDL_Rect *cameraBox);

		/// Pointer to the tilesheet where to choose which tile to draw from
		SDL_Surface *tileSheet;

	public:

		/// Represent the tile
		int tileID;
		/// Represent the kind of tile
		int typeID;

	public:

		/// Default constructor
		HB_Tile(int x, int y, int type, int ID, SDL_Surface *tileSheet);

		/// Draw the tile onto a destination SDL_Surface, choosing wich one to draw
		/// from the clip passed
		void draw(SDL_Surface *destination,  SDL_Rect *clip);
};