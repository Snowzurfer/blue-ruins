#include "HB.h"
#include "HB_Surface.h"

/// The constructor
HB::HB()
{
	// Reset all the various surfaces
	screen = NULL;
	deltaTime = NULL;
	

	// Set the game on running
	running = true;

	
	
	// Reset the frame counter
	totalFrames = 0;

	//testCamera = boost::shared_ptr<HB_Camera>(new HB_Camera(0, 0, NULL, NULL));
}

int HB::run()
{
	// Initialize
	if(init() == false)
	{
		return -1; // If initialization failed, exit the game
	}

	// Create the objects contained into the game
	create_objects();

	// Load the files
	if(load_files() == false)
	{
		return false; // If loading failed, exit the game
	}

	// Start the frame regulator timer
	deltaTime->start();


	// While the user has not quit (The main Loop)
	while(running)
	{

		// If there is an event to handle
		while(SDL_PollEvent (&eventu))
		{
			// Handle the input events
			events(&eventu);
		}

		// Update the engine
		update(deltaTime->get_ticks());

		// Restart the timer
		deltaTime->start();

		// Draw the updated game
		draw();

		// Increase the frames counter
		totalFrames ++;
	}

	// If the user quit the game, handle the memory clean up and quit everything
	clean_up();

	return 0;
}


