#pragma once

#include "HB_Global.h"
#include "HB_Entity.h"
#include "HB_Map.h"

class HB_EntitiesManager
{
	public:

		// Default constructor
		HB_EntitiesManager();

		// Default destructor
		~HB_EntitiesManager();

		// Handle user input
		void handle_events(SDL_Event *events);

		// Update the entities
		void update(double *deltaTicks);

		// Draw the entities onto a destination SDL_Surface
		void draw(SDL_Surface *destination);

		// Push an enttity into the pool
		void add_entity(HB_Entity *entity);
		
		// Wheter the user has lost or not
		bool playerDead;

		// Number of enemies which have been killed
		int enemiesDead;

};