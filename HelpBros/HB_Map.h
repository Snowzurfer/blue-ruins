#pragma once

#include "HB_Global.h"
#include "HB_Surface.h"
#include "HB_Tile.h"
#include "HB_Background.h"
#include "TMXParser\TMXLoader.h"
#include <vector>

class HB_Map
{
	private:

		// Pool containing all the tiles of the map
		std::vector<HB_Tile*> tileList;

		// Width in tiles
		int widthT; 
		// Height in tiles
		int heightT;

		TMXLoader *mapLoader;

	public:

		// SDL_Surface representing the map
		SDL_Surface *tilesetSurface;

		// Background for that map
		HB_Background *background;

		static HB_Map mapControl;


	public:

		// Default constructor
		HB_Map();

		// Default deconstructor
		~HB_Map();

		// Load the map from file
		bool load_map(std::string mapFile);

		// Draw the map onto a destination SDL_Surface
		void draw(SDL_Surface *destination);

		// Update the map
		void update();

		// Returns the tile in the given position
		HB_Tile *get_tile(int x, int y);

		int heightPX, widthPX;
};