#include "HB_Font.h"

HB_Font::HB_Font(int fontSize, int R, int G, int B)
{
	// Set the font
	font = TTF_OpenFont("./content/font/coders_crux.ttf", fontSize);
	// font = TTF_OpenFont("./content/font/backtobay.ttf", fontSize);

	// Set the color of the font
	fontColor.b = B;
	fontColor.g = G;
	fontColor.r = R;

	// Set the background color
	backgroundColor.b = 0;
	backgroundColor.g = 0;
	backgroundColor.r = 0;

	// Set the font size
	Size = fontSize;
}

void HB_Font::LoadFont(char *name)
{
	font = TTF_OpenFont(name, Size);
}

void HB_Font::draw(int x,int y,char *text,SDL_Surface *screen)
{
	textSurface = TTF_RenderText_Shaded(font, text, fontColor, backgroundColor);
	pos.x = x;
	pos.y = y;
	SDL_BlitSurface(textSurface, NULL, screen, &pos);
	SDL_FreeSurface(textSurface);
}

HB_Font::~HB_Font()
{
	TTF_CloseFont(font);
}