#include "HB.h"

/// Loads the files and returns false if there were any errors
bool HB::load_files()
{	
	if(statesManager->load_files() == false)
	{
		return false;
	}

	return true;
}