#include "HB_Map.h"


HB_Map HB_Map::mapControl;

HB_Map::HB_Map()
{
	// Reset the surface
	tilesetSurface = NULL;


	background = new HB_Background();
	mapLoader = NULL;

	// Set the width and the height of the map
	widthT = 0;
	heightT = 0;
}

HB_Map::~HB_Map()
{
	for(int i = 0; i < (int)tileList.size(); i++)
	{
		delete tileList[i];
	}

	SDL_FreeSurface(tilesetSurface);

	delete mapLoader;
}

bool HB_Map::load_map(std::string mapFile)
{
	// Reset the tile list
	tileList.clear();

	mapLoader = new TMXLoader();

	mapLoader->loadDocument(mapFile);

	// Read widht and height in tiles
	widthT = mapLoader->getNumMapColumns();
	heightT = mapLoader->getNumMapRows();


	/* This procedure is needed since the paths Tiled used are different
		from the ones used by the program*/
	string tilesetPath = mapLoader->getTilesetPath();
	tilesetPath.insert(2, "./content");
	tilesetPath.erase(tilesetPath.begin(), tilesetPath.begin() + 2);
	string backgroundPath = mapLoader->getBGPath();
	backgroundPath.insert(2, "./content");
	backgroundPath.erase(backgroundPath.begin(), backgroundPath.begin() + 2);

	// Load the tileset surface
	if((tilesetSurface = HB_Surface::load_image(tilesetPath)) == NULL)
	{
		return false;
	}

	// Load the background surface
	if(background->load_image(backgroundPath, mapLoader->getBackgroundData(), widthT, heightT) == false)
	{
		return false;
	}

	// Create a matrix containing the tile map
	vector< vector< int > > tilesetDataTemp = mapLoader->getTilesetData();

	// Used to create new tiles, hold tiles' ID and type
	int tileIDTemp = 0;
	int tileTypeTemp = 0;
	// Load the tiles
	for(int r = 0; r < heightT; r++)
	{
		for(int c = 0; c <widthT; c++)
		{
			// Read the graphic ID of this tile
			tileIDTemp = tilesetDataTemp[r][c];

			// If the tile type is NULL
			if(tileIDTemp == 0)
			{
				tileTypeTemp = TILE_NONE;
			}
			else
			{
				if(tileIDTemp > 3)
				{
					int LOL = 0;
				}
				
				// Trasform into a 0-coordinates useful variable
				tileIDTemp -= 2;

				tileTypeTemp = TILE_BLOCK;
			}

			// Create the tile
			HB_Tile *tempTile = new HB_Tile(c * TILE_SIZE, r * TILE_SIZE, tileTypeTemp,
												tileIDTemp, tilesetSurface);
			// Push it back into the list
			tileList.push_back(tempTile);
		}
	}

	// Set the bounds of level for the camera
	HB_Camera::cameraControl.set_levelBounds(widthT * TILE_SIZE, heightT * TILE_SIZE);

	widthPX = widthT * TILE_SIZE;
	heightPX = heightT * TILE_SIZE;

	return true;
}

void HB_Map::draw(SDL_Surface *destination)
{
	// Draw the background
	background->draw(destination);

	// IF the the surface of the map exists
	if( tilesetSurface != NULL)
	{
		// Calculate the width and the height in tiles in the tile sheet
		int tilesetWidthT = tilesetSurface->w / TILE_SIZE;
		int tilesetHeightT = tilesetSurface->h / TILE_SIZE;

		// Counter used to scan the tiles list
		int ID = 0;

		// Coordinates where to clip the tile set sprite sheet
		SDL_Rect clip;

		// For each column
		for(int y = 0; y < heightT; y++)
		{
			// For each element in a row, for each column
			for(int x = 0; x < widthT; x++)
			{
				// If the tile with that ID must not be drawn
				if(tileList[ID]->typeID == TILE_NONE)
				{
					// Icrease the ID counter
					ID ++;

					// Skip to the next loop, as this tile
					// must not be drawn
					continue;
				}

				// Determine which clip from the tile set sprite sheet to use
				clip.x = (tileList[ID]->tileID % tilesetWidthT) * TILE_SIZE;
				clip.y = (tileList[ID]->tileID / tilesetWidthT) * TILE_SIZE;
				clip.w = TILE_SIZE;
				clip.h = TILE_SIZE;

				// Now draw the tile
				tileList[ID]->draw(destination, &clip);

				// Icrease the ID counter
				ID ++;
			}
		}
	}
}

HB_Tile *HB_Map::get_tile(int x, int y)
{
	// Retrieve the tile ID
	int ID = x / TILE_SIZE;
		ID = ID + ((y / TILE_SIZE) * widthT);

	// If the tile the function is searching for does not exist
	if(ID < 0 || ID > tileList.size())
	{
		return NULL;
	}

	// Return the searched map
	return tileList[ID];
}

void HB_Map::update()
{
	background->update();
}