#include "HB_InGame.h"


HB_InGame::HB_InGame(HB_Timer *deltaTime)
{
	// The developer info are shown by default, for the moment
	showConsole = true;
	
	map = new HB_Map();
	eManag = new HB_EntitiesManager();
	player1 = new HB_Player(32, 32, map);
	HB_Camera::cameraControl.set_target(&player1->x, &player1->y);

	eManag->add_entity(player1);

	console = new HB_Console(deltaTime, &player1->x, &player1->y, &player1->xVel, &player1->yVel);

	enemiesSpawner = new HB_EnemiesManager(map);

	changeState = false;

	testBullet = new HB_Bullet(100, 230, DIRECTION_RIGHT, map);

	score = &eManag->enemiesDead;

	hud = new HB_HUD(score);

	nextStateID = STATE_NULL;

	
}


HB_InGame::~HB_InGame(void)
{
	delete console;
	delete eManag;
	delete map;
	delete enemiesSpawner;
	delete hud;
}

bool HB_InGame::load_files()
{
	if( player1->load_files() == false)
	{
		return false;
	}

	if( map->load_map("./content/maps/800x800_test.tmx") == false)
	{
		return false;
	}

	console->load_files();

	hud->load_files();

	/*	if(testMap->load_map("./content/maps/1.map") == false)
	{
		return false;
	}*/

	/* Load the music
	music = Mix_LoadMUS( "beat.wav" );

	// Load the sound effects
	scratch = Mix_LoadWAV("scratch.wav");
	high = Mix_LoadWAV("high.wav");
	med = Mix_LoadWAV("medium.wav");
	low = Mix_LoadWAV("low.wav");*/

	
	/*// If there was a problem loading the music
	if(music == NULL)
		return false;

	// If there was a problem loading the sound effects+
	if( (scratch == NULL) || (high == NULL) || (med == NULL) || (low == NULL) )
		return false;*/

	// If everything loaded fine
	return true;
}

void HB_InGame::update(double *deltaTicks)
{
	// If the player is dead
	if(eManag->playerDead)
	{
		nextStateID = STATE_GAMEOVER;

		changeState = true;
	}
	else
	{
		enemiesSpawner->update();

		eManag->update(deltaTicks);

		HB_Camera::cameraControl.update();

		map->update();

		console->update(deltaTicks);
	}
}

void HB_InGame::handle_events(SDL_Event *Event)
{
	// Update the entities
	eManag->handle_events(Event);
}

void HB_InGame::draw(SDL_Surface *destination)
{
	map->draw(destination);

	eManag->draw(destination);


	// If the console must be shown
	if(showConsole)
	{
		// Draw the console
		console->draw(destination);
	}

	hud->draw(destination);
}