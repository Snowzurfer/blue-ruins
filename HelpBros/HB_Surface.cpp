#include "HB_Surface.h"

void HB_Surface::draw_surface(SDL_Surface *destination, int dX, int dY, SDL_Surface *source, SDL_Rect *clipz)
{
	// If the source or the destination SDL_Surface is missing, exit the function
	if(source == NULL || destination == NULL)
	{
		return;
	}

	// Make a temporary rectangle to hold the offsets
	SDL_Rect offset;
	// Give the offsets to the rectangle
	offset.x = dX;
	offset.y = dY;

	// Render it to the destination
	SDL_BlitSurface( source, clipz, destination, &offset );

}

SDL_Surface *HB_Surface::load_image( std::string filename )
{
	// Temporary storage for the image that is loaded
	SDL_Surface *loadedImage = NULL;

	// The optimized image that will be used
	SDL_Surface *optimizedImage = NULL;

	// Load the image using SDL_image
	loadedImage = IMG_Load( filename.c_str() );

	// If the image was loaded correctly
	if(loadedImage != NULL)
	{
		// Create an optimized image
		optimizedImage = SDL_DisplayFormat( loadedImage );

		// Free the old image
		SDL_FreeSurface( loadedImage );

		// IF the image was optimized just fine
		if( optimizedImage != NULL)
		{
			// Map the color key to be #FF00FF (255, 000, 255)
			double colorkey = SDL_MapRGBA( optimizedImage->format, 0xFF, 0x00, 0xFF, 0x00 );
				
			// Set all the pixels of that colorkey to be transparent when blitted
			SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
		}
		
		// Return the optimized image
		return optimizedImage;
	}

	// Otherwhise it does not return anything (NULL)
	return NULL;
}