#include "HB_GameStatesManager.h"


HB_GameStatesManager::HB_GameStatesManager(HB_Timer *deltaTime)
{
	currentStateID = STATE_TITLE;
	nextStateID = STATE_NULL;

	currentState = new HB_Title();

	this->deltaTime = deltaTime;
}


HB_GameStatesManager::~HB_GameStatesManager(void)
{
	delete currentState;
}

void HB_GameStatesManager::events(SDL_Event *Event)
{
	// Handle events for the current state
	currentState->handle_events(Event);
}

void HB_GameStatesManager::update(double *deltaTicks)
{
	// If the game must quit, skip the update
	if(currentStateID != STATE_EXIT)
	{
		// Update the current state
		currentState->update(deltaTicks);

		// If the state must be changed
		if(currentState->changeState)
		{
			// Change it
			set_next_state(currentState->nextStateID);

			change_state();
		}
	}
}

void HB_GameStatesManager::draw(SDL_Surface *destination)
{
	// Render the current state
	currentState->draw(destination);
}

bool HB_GameStatesManager::load_files()
{
	// If there were problems loading the level
	if(currentState->load_files() == false)
	{
		return false;
	}

	return true;
}

void HB_GameStatesManager::set_next_state(int newState)
{
	// If the user doesn't want to exit
	if(nextStateID != STATE_EXIT)
	{
		// Set the next state
		nextStateID = newState;
	}
}

void HB_GameStatesManager::change_state()
{
	// If the state must be changed
	if(nextStateID != STATE_NULL)
	{
		// Delete the current state
		if(nextStateID != STATE_EXIT)
		{
			delete currentState;
		}

		// Change the state
		switch(nextStateID)
		{
		case STATE_TITLE:
			currentState = new HB_Title();
			currentState->load_files();
			break;
		case STATE_INGAME:
			currentState = new HB_InGame(deltaTime);
			currentState->load_files();
			break;
		case STATE_MENU:
			currentState  = new HB_MainMenu();
			currentState->load_files();
			break;
		/*case STATE_INTRO:
			currentState = new HB_Intro();
			break;*/
		case STATE_GAMEOVER:
			currentState = new HB_GameOver();
			currentState->load_files();
			break;
		}

		// Change the current state ID
		currentStateID = nextStateID;

		// Set the next state ID to null
		nextStateID = STATE_NULL;
	}
}