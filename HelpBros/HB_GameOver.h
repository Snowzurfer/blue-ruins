#pragma once

#include "HB_Global.h"
#include "HB_GameState.h"


class HB_GameOver : public HB_GameState
{
	public:

		HB_GameOver(void);

		~HB_GameOver(void);

		void handle_events(SDL_Event *Events);

		void update(double *deltaTicks);

		void draw(SDL_Surface *destination);

		bool load_files();

	private:

		// The font used for the first message
		TTF_Font *font0;
		SDL_Surface *message0;

		// The font used for the second message
		TTF_Font *font1;
		SDL_Surface *menuMessage0;
		SDL_Surface *menuMessage1;
		SDL_Surface *menuMessage2;


		SDL_Color *textColor;

		// The title background
		SDL_Surface *background;

		// Used to determine when the player relases the button
		bool returnPressed;
};

