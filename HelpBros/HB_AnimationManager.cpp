#include "HB_AnimationManager.h"

HB_AnimationManager::HB_AnimationManager()
{
	current_animation = 0;
}

HB_AnimationManager::~HB_AnimationManager()
{
	// Free the pool
	for(int i = 0; i < animationsPool.size(); i++)
	{
		delete animationsPool[i];
	}
}

void HB_AnimationManager::update()
{
	// Update the current animation
	animationsPool[current_animation]->update();
}

void HB_AnimationManager::draw(SDL_Surface *destination)
{
	// Draw the current animation
	animationsPool[current_animation]->draw(destination);
}

bool HB_AnimationManager::play_animation(std::string name)
{
	// Search for that animation
	for(int i = 0; i < animationsPool.size(); i++)
	{
		// If the name corresponds
		if(animationsPool[i]->name.compare(name) == 0)
		{
			// Set it as the animation to be played
			current_animation = i;

			return true;
		}
	}

	// Animation not found
	return false;
}

void HB_AnimationManager::add_animation(std::string name, int numFrames, int w, int h, float *entityX, 
							float *entityY)
{
	// Create a an animation
	HB_Animation *animation = new HB_Animation(name, numFrames, w, h, entityX, entityY);

	// Push it into the pool
	animationsPool.push_back(animation);
}

bool HB_AnimationManager::load_image(std::string path, std::string animName)
{
	// Search for that animation
	for(int i = 0; i < animationsPool.size(); i++)
	{
		// If the name corresponds
		if(animationsPool[i]->name.compare(animName) == 0)
		{
			// Load the image
			if(animationsPool[i]->load_image(path) == false)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}

	return false;
}
