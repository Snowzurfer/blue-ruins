#pragma once

#include "HB_Global.h"
#include "HB_GameState.h"
#include "HB_Map.h"
#include "HB_Player.h"
#include "HB_Camera.h"
#include "HB_EntitiesManager.h"
#include "HB_Console.h"
#include "HB_EnemiesManager.h"
#include "HB_Bullet.h"
#include "HB_HUD.h"

class HB_InGame :
	public HB_GameState
{
	public:

		// Constructor
		HB_InGame(HB_Timer *deltaTime);

		// Deconstructor
		~HB_InGame(void);

		void handle_events(SDL_Event *Events);

		void update(double *deltaTicks);

		void draw(SDL_Surface *destination);

		bool load_files();

	private:

		HB_EntitiesManager *eManag;

		HB_Player *player1;

		// The enemies manager
		HB_EnemiesManager *enemiesSpawner;

		HB_Map *map;

		/// Wheter or not the developer info about the game must be shown
		bool showConsole;

		/// The developer console
		HB_Console *console;

		// Test bullet
		HB_Bullet *testBullet;

		// The HUD
		HB_HUD *hud;

	private:

		int *score;


};

