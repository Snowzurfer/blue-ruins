#include "HB_Area.h"

HB_Area::HB_Area()
{
	areaSize = 0;

	surfaceTileset = NULL;
}

HB_Area::~HB_Area()
{

}

bool HB_Area::load_files(char *file)
{
	// Reset the map list
	mapList.clear();

	FILE *FH = NULL;
	FH = std::fopen(file, "r");

	if(FH == NULL)
	{
		std::fclose(FH);

		return false;
	}

	char tileSetFile[255];

	std::fscanf( FH, "%s\n", tileSetFile);

	// Load the tileset image
	if((surfaceTileset = HB_Surface::load_image(tileSetFile)) == false)
	{
		std::fclose(FH);

		return false;
	}

	std::fscanf( FH, "%d\n", &areaSize);


	char mapFile[255];



	for(int x = 0; x < areaSize; x ++)
	{
		for(int y = 0; y < areaSize; y ++)
		{
			std::fscanf(FH, "%s", mapFile);

			HB_Map tempMap(40, 40);

			if(tempMap.load_map(mapFile) == false)
			{
				std::fclose(FH);

				return false;
			}

			tempMap.tilesetSurface = surfaceTileset;

			mapList.push_back(tempMap);
		}
		std::fscanf(FH, "\n");
	}

	std::fclose(FH);

	return true;
}

void HB_Area::draw(SDL_Surface *destination, int cameraX, int cameraY)
{
	int mapWidth = 40 * TILE_SIZE;
	int mapHeight = 40 * TILE_SIZE;

	// The first map to be drawn, calculated basing onto the camera's positions
	int firstID = -cameraX / mapWidth;
		firstID = firstID + (( -cameraY / mapHeight) * areaSize);

	int ID = 0;
	int X = 0;
	int Y = 0;

	// Now loop through the 4 maps that must be rendered
	for( int i = 0; i < 4; i++)
	{
		ID = firstID + ((i /2) * areaSize) + ( i % 2);

		// If the calculated ID does not exist
		if( ID < 0 || ID >= mapList.size())
		{
			// Skip the loop
			continue;
		}

		X = (( ID % areaSize) * mapWidth) + cameraX;
		Y = (( ID / areaSize) * mapHeight) + cameraY;

		// Draw the map
		mapList[ID].draw(destination);
	}
}

void HB_Area::on_cleanup()
{
	SDL_FreeSurface(surfaceTileset);

	mapList.clear();
}

HB_Map *HB_Area::get_map(int x, int y, int mapW, int mapH)
{
	// Retrieve the map ID
	int ID = x / mapW;
		ID = ID + ((y / mapH) * areaSize);

	// If the map the function is searching for does not exist
	if(ID < 0 || ID > mapList.size())
	{
		return NULL;
	}

	// Return the searched map
	return &mapList[ID];
}

HB_Tile *HB_Area::get_tile(int x, int y)
{
	// Calculate dimensions of the map
	int mapWidth = 40 * TILE_SIZE;
	int mapHeight = 40 * TILE_SIZE;

	// Retrieve the map where the tile is into
	HB_Map *tempMap = get_map(x, y, mapWidth, mapHeight);

	// If the map where we are searching the tile into does not exist
	if(tempMap == NULL)
	{
		return NULL;
	}

	x = x % mapWidth;
	y = y % mapHeight;

	return tempMap->get_tile(x, y);
}