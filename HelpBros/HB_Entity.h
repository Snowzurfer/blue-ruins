#pragma once

#include "HB_Global.h"
#include <vector>
#include "HB_Surface.h"
#include "HB_Animation.h"
//#include "HB_EntityCollisions.h"
#include "HB_Map.h"


enum { // TODO: OPTIMIZE
    ENTITY_TYPE_GENERIC = 0,
    ENTITY_TYPE_PLAYER,
	ENTITY_TYPE_ENEMY,
	ENTITY_TYPE_BULLET,
	ENTITY_TYPE_BOLT
};

enum
{
	ENEMY_RED = 0,
	ENEMY_GREEN,
	ENEMY_BOT
};

enum { // TODO: OPTIMIZE
	ENTITY_FLAG_NONE        = 0,
	ENTITY_FLAG_GRAVITY    = 0x00000001,
    ENTITY_FLAG_GHOST    = 0x00000002,
    ENTITY_FLAG_MAPONLY    = 0x00000004
};

enum states
{
	ENTITY_STATE_ALIVE = 0,
	ENTITY_STATE_DYING,
	ENTITY_STATE_DEAD,
	ENTITY_STATE_BOLTED,
	ENTITY_STATE_FIRING,
	ENTITY_STATE_BOLTING,
	ENTITY_STATE_CROUCHING
};





class HB_Entity
{
	public:

		// Pool containing smart pointers to the entities alive
		static std::vector<HB_Entity*> EntitiesPool;

		// Coordinates and dimension of the Entity
		float x, y;
		int width, height;

		// Whether this entity is the player or a generic one
		int type;

		// Whether the player is moving right or left
		bool movingRight, movingLeft;

		// Wheter the player is dead or not
		bool dead;

		int flags;

		int offsetX, offsetY, offsetW, offsetH;

		// Velocities of the entity
		float xVel, yVel;

		bool canJump;
		bool jumpKeyReleased;

		// Wheter the player is stationary or not
		bool idle;

		bool onGround;

		bool falling;

		bool jumping;

		int collidedLR;

		// The current state of the entity
		int state;

		// Holds the last state
		int prevState;

		// Used inside "pos_valid", they are declared here for performance purposes
		int startX, startY, endX, endY;

	protected:

		// Accelleration of the player
		float xAcc, yAcc;
		
		// Maximum velocities of the entity
		float maxVelX, maxVelY;

		// The constant factor of accelleration
		float xAccFactor;

		// The constant factor of decelleration
		float xDecFactor;

		// Health Points of the enemy
		int HP;

		double speedFactor;
		
		//HB_Collisions *collisions;

		/* These variables represent the bounding boxes
		 of the two entities. They are declared here, as this
		 way they has not to be declared each time the function
		 is called (collision_check)*/
		SDL_Rect *entityARect;
		/* These variables represent the bounding boxes
		 of the two entities. They are declared here, as this
		 way they has not to be declared each time the function
		 is called (collision_check)*/
		SDL_Rect *entityBRect;

		// Pointer to the current map
		HB_Map *mapReference;

		double ppStepX; // TODO: OPTIMIZE
		double ppStepY; // TODO: OPTIMIZE

		// Timer which allows the animation to play
		HB_Timer *dyingAnimTimer;
		// The amount of time this animation takes
		double dyingAnimTime;

		// Timer which allows the animation to play
		HB_Timer *boltedAnimTimer;
		// The amount of time this animation takes
		double boltedAnimTime;
		bool setAnimBack;

		// The last direction the entity was facing to
		int lastDirection;

	protected:

		// Decides wheter the entity can jump or not
		bool jump();

	public:

		/// Default constructor
		HB_Entity(int Width, int Height, HB_Map *mapReference); 

		/// Default deconstructor
		virtual ~HB_Entity();

		/// Handle user input
		virtual void handle_events(SDL_Event *events);

		/// Update the entity
		virtual void update(double *deltaTicks);

		/// Draw the entity onto a destination SDL_Surface
		virtual void draw(SDL_Surface *Dest);

		/// Happens when this entity has collided with another one
		virtual bool on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis);

		// Happens when this entity collides with the map for some reason
		virtual bool on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *axis);

	public:

		/// Stop the entity from moving gradually
		void stop_moving();

		// Hurt the entity for a certain amount of health points
		void hurt(int HP);

		/// Move the entity and make a pixel-per-pixel check before moving
		void move(double *deltaTicks);

		///<summary>
		/// Check wheter the entity can move to the next position (or step)
		///</summary>
		bool pos_valid(float stepX, float stepY, int direction);

		/* Used to determine where an entity wants to move to.
			They basically represent the x and y velocity of the entity;
			they are allocated here in order to not have to allocate them
			each time move is called. */
		float moveX, moveY;

		// Check if entity A collided with entity B
		bool collision_check(HB_Entity *entityB, float *stepX, float *stepY);

		// Used within pos_valid, deterimine if the entity can move on a certain tile
		bool pos_valid_tile(HB_Tile *tile);

		// Used within pos_valid, deterimine if the entity collided whith a certain one
		bool pos_valid_entity(HB_Entity *entityA, HB_Entity *entityB, float *stepX, float *stepY, int axis);


};

class HB_EntityCollisions
{
	public:

		static std::vector<HB_EntityCollisions*> entityColList; // TODO: OPTIMIZE

	public:

		HB_Entity *entityA;
		HB_Entity *entityB;

		// The directions where the entity was moving to
		float moveX, moveY;

		int axis;

	public:

		HB_EntityCollisions(HB_Entity *entityA, HB_Entity *entityB, float moveX, float moveY, int axis);

		~HB_EntityCollisions();
};

class HB_MapCollisions
{
	public:

		// The pool containing the collisions
		static std::vector<HB_MapCollisions*> mapColPoll;

		// Constructor
		HB_MapCollisions(HB_Entity *entity, HB_Tile *tile, float moveX, float moveY, int axis);

		// Deconstructor
		~HB_MapCollisions(void);

	public:

		// The entity which has collided
		HB_Entity *entity;

		// The directions where the entity was moving to
		float moveX, moveY;

		// The tile which has collided with the entity
		HB_Tile *tile;

		// The direction where to the entity was moving before colliding
		int axis;
};
