#include "HB_Global.h"
#include "HB.h"

void HB::draw()
{
	// Cover the screen of black for avoiding surface endings problems
	SDL_FillRect(screen,NULL,0);

	// Draw the current state
	statesManager->draw(screen);
	
	// Update the screen
	SDL_Flip( screen );
}

