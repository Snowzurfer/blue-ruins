#include "HB_Entity.h"

std::vector<HB_MapCollisions*> HB_MapCollisions::mapColPoll;

HB_MapCollisions::HB_MapCollisions(HB_Entity *entity, HB_Tile *tile, float moveX, float moveY, int directions)
{
	this->entity = entity;
	this->tile = tile;
	this->moveX = moveX;
	this->moveY = moveY;
	this->axis = directions;
}


HB_MapCollisions::~HB_MapCollisions(void)
{
}
