#pragma once

#include "HB_Global.h"
#include "HB_Entity.h"
#include "HB_AnimationManager.h"
#include <stdio.h>
#include <ctime>

class HB_EnemyGreen : public HB_Entity
{
	private:

		// The animations of this enemy
		HB_AnimationManager *animations;

	private:

		/* All these variables are used to spawn the enemy with a random velocity*/
		float rangeMin;
		float rangeMax;
		
		bool pixelMoved;

	public:

		// Constructor
		HB_EnemyGreen(int xPos, int yPos, HB_Map *mapReference);

		~HB_EnemyGreen(void);

		/// Load the animation
		bool load_files();

		/// Update the enemy data
		void update(double *deltaTicks);
		
		/// Draw the enemy
		void draw(SDL_Surface *destination);

		// Happens when this entity has collided with another one
		bool on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis);

		// Happens when this entity has collided with the map
		bool on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *direction);

		void handle_events(SDL_Event *events);

		// Wheter to play or not the falling animation
		bool fallingPlayed;

		bool moveRPlayed, moveLPlayed, dyingPlayed, boltedPlayed;

		int enemyType;
};

