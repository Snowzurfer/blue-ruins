#include "HB_Player.h"

HB_Player::HB_Player(int Width, int Height, HB_Map *mapReference)
	:HB_Entity(Width, Height, mapReference)
{
	// Create the animation
	//movingAnim = new HB_Animation("MoveR", spriteFrames, Width, Height, &x, &y);

	animations = new HB_AnimationManager();
	
	animations->add_animation("IdleR", 1, Width, Height, &x, &y);
	animations->add_animation("MoveR", 8, Width, Height, &x, &y);
	animations->add_animation("MoveL", 8, Width, Height, &x, &y);
	animations->add_animation("JumpRU", 1, Width, Height, &x, &y);
	animations->add_animation("JumpRD", 1, Width, Height, &x, &y);
	animations->add_animation("JumpLU", 1, Width, Height, &x, &y);
	animations->add_animation("JumpLD", 1, Width, Height, &x, &y);
	animations->add_animation("IdleL", 1, Width, Height, &x, &y);
	animations->add_animation("CrouchL", 1, Width, Height, &x, &y);
	animations->add_animation("CrouchR", 1, Width, Height, &x, &y);
	animations->add_animation("FireL", 1, Width, Height, &x, &y);
	animations->add_animation("FireR", 1, Width, Height, &x, &y);

	offsetX = 8;
	offsetW = 6;

	maxVelX = 6;
	maxVelY = 17;
	xAccFactor = 1;
	xDecFactor = 1.5f;

	jumpKeyReleased, fireButtonRelased, boltingButtonRelased = true;

	lastDirection = 0;

	x = 100;
	y = 600;

	fallingPlayed = idlePlayed = moveLPlayed = moveRPlayed = jumpPlayed = crouching = boltingPlayed = false;

	type = ENTITY_TYPE_PLAYER;

	HP = 3;

	hurtTime = 5000;

	unactive = false;

	hurtTimer = new HB_Timer();

	hurtTime = 0;

	dyingAnimTime = 1000;

	firing = Bolting = false;

	bolt = NULL;
}

HB_Player::~HB_Player()
{
	//delete movingAnim;

	delete animations;

	
}

void HB_Player::draw(SDL_Surface *destination)
{
	HB_Entity::draw(destination);

	//movingAnim->draw(destination);

	animations->draw(destination);
}

bool HB_Player::load_files()
{
	// Load the idle animation sprite
	if(animations->load_image("./content/gfx/Hero_Idle_Right.png", "IdleR") == false)
	{
		return false;
	}
	// Load the idle animation sprite
	if(animations->load_image("./content/gfx/Hero_Idle_Left.png", "IdleL") == false)
	{
		return false;
	}
	// Load the walking animation sprite
	if(animations->load_image("./content/gfx/Hero_Right.png", "MoveR") == false)
	{
		return false;
	}
	// Load the walking animation sprite
	if(animations->load_image("./content/gfx/Hero_Left.png", "MoveL") == false)
	{
		return false;
	}
	// Load the jumping animation sprite
	if(animations->load_image("./content/gfx/Hero_Jump_Right_Down.png", "JumpRD") == false)
	{
		return false;
	}
	// Load the jumping animation sprite
	if(animations->load_image("./content/gfx/Hero_Jump_Right_Up.png", "JumpRU") == false)
	{
		return false;
	}
	// Load the jumping animation sprite
	if(animations->load_image("./content/gfx/Hero_Jump_Left_Down.png", "JumpLD") == false)
	{
		return false;
	}
	// Load the jumping animation sprite
	if(animations->load_image("./content/gfx/Hero_Jump_Left_Up.png", "JumpLU") == false)
	{
		return false;
	}
	// Load the crouching animation sprite
	if(animations->load_image("./content/gfx/Hero_Crouch_Left.png", "CrouchL") == false)
	{
		return false;
	}
	// Load the crouching animation sprite
	if(animations->load_image("./content/gfx/Hero_Crouch_Right.png", "CrouchR") == false)
	{
		return false;
	}
	// Load the firing animation sprite
	if(animations->load_image("./content/gfx/Hero_Fire_Left.png", "FireL") == false)
	{
		return false;
	}
	// Load the firing animation sprite
	if(animations->load_image("./content/gfx/Hero_Fire_Right.png", "FireR") == false)
	{
		return false;
	}
	

	return true;
}

void HB_Player::update(double *deltaTicks)
{

	// If the hurt time has passed
	if(hurtTimer->get_ticks() >= hurtTime)
	{
		// Take the entity back to normal condition
		hurtTimer->stop();
		unactive = false;
	}

	HB_Entity::update(deltaTicks);
	
	if(firing)
	{
		fire();
	}
	else if(Bolting)
	{
		bolting();
	}
		

	/*if(xVel != 0)
	{
		int LOL = 0;
	}*/
	// If the entity has not been killed and it's playing the dying animation
	if(state != ENTITY_STATE_DYING)
	{

		if(xVel > 0 && canJump && !moveRPlayed)
		{
			moveRPlayed = true;
			moveLPlayed = false;
			idlePlayed = false;
			fallingPlayed = false;
			jumpPlayed = false;
			crouchPlayed = false;
			boltingPlayed = false;

			animations->play_animation("MoveR");
		}
		else if(xVel < 0 && canJump && !moveLPlayed)
		{
			moveLPlayed = true;
			moveRPlayed = false;
			idlePlayed = false;
			fallingPlayed = false;
			jumpPlayed = false;
			crouchPlayed = false;
			boltingPlayed = false;

			animations->play_animation("MoveL");
		}
		else if( xVel == 0 && crouching && !crouchPlayed && canJump && (state != ENTITY_STATE_BOLTING && state != ENTITY_STATE_FIRING))
		{
			moveLPlayed = false;
			moveRPlayed = false;
			idlePlayed = false;
			fallingPlayed = false;
			jumpPlayed = false;
			crouchPlayed = true;
			boltingPlayed = false;

			if(lastDirection == 0) // Right
			{
				animations->play_animation("CrouchR");
			}
			else // Left
			{
				animations->play_animation("CrouchL");
			}
		}
		else if( xVel == 0 && canJump && !idlePlayed && !crouching && (state != ENTITY_STATE_BOLTING && state != ENTITY_STATE_FIRING))
		{
			moveLPlayed = false;
			moveRPlayed = false;
			idlePlayed = true;
			fallingPlayed = false;
			jumpPlayed = false;
			crouchPlayed = false;
			boltingPlayed = false;

			if(lastDirection == 0) // Right
			{
				animations->play_animation("IdleR");
			}
			else // Left
			{
				animations->play_animation("IdleL");
			}
		}
		else if( xVel == 0 && (state == ENTITY_STATE_BOLTING || state == ENTITY_STATE_FIRING) && canJump && !boltingPlayed)
		{
			moveLPlayed = false;
			moveRPlayed = false;
			idlePlayed = false;
			fallingPlayed = false;
			jumpPlayed = false;
			crouchPlayed = false;
			boltingPlayed = true;

			if(lastDirection == 0) // Right
			{
				animations->play_animation("FireR");
			}
			else // Left
			{
				animations->play_animation("FireL");
			}
		}
		else if( yVel > 0 && !canJump && !fallingPlayed)
		{
			fallingPlayed = true;
			jumpPlayed = false;
			moveLPlayed = false;
			moveRPlayed = false;
			idlePlayed = false;
			crouchPlayed = false;
			boltingPlayed = false;

			if(lastDirection == 0) // Right
			{
				animations->play_animation("JumpRD");
			}
			else // Left
			{
				animations->play_animation("JumpLD");
			}
		}
		else if( yVel < 0 && !canJump && !jumpPlayed)
		{
			fallingPlayed = false;
			jumpPlayed = true;
			moveLPlayed = false;
			moveRPlayed = false;
			idlePlayed = false;
			crouchPlayed = false;
			boltingPlayed = false;

			if(lastDirection == 0) // Right
			{
				animations->play_animation("JumpRU");
			}
			else // Left
			{
				animations->play_animation("JumpLU");
			}
		}
	}
		
	animations->update();
}

void HB_Player::handle_events(SDL_Event *events)
{
	// If a key was pressed
	if(events->type == SDL_KEYDOWN)
	{
		// If the player must jump
		if(events->key.keysym.sym == SDLK_z)
		{
			if(jumpKeyReleased)
			{
				jump();
				jumpKeyReleased = false;
			}
		}
		else if(events->key.keysym.sym == SDLK_x)
		{
			if(fireButtonRelased)
			{
				firing = true;
				/*prevState = state;
				state = ENTITY_STATE_FIRING;*/
				fireButtonRelased = false;
			}
		}
		else if(events->key.keysym.sym == SDLK_c)
		{
			if(boltingButtonRelased)
			{
				Bolting = true;
				//prevState = state;
				state = ENTITY_STATE_BOLTING;
				boltingButtonRelased = false;
			}
		}
		else if(events->key.keysym.sym == SDLK_DOWN)
		{
			if(state != ENTITY_STATE_BOLTING)
			{
				crouching = true;
				prevState = state;
				state = ENTITY_STATE_CROUCHING;
			}
		}
		if(events->key.keysym.sym == SDLK_RIGHT)
		{
			movingRight = true;
			lastDirection = 0;
		}
		else if(events->key.keysym.sym == SDLK_LEFT)
		{
			movingLeft = true;
			lastDirection = 1;
		}
		

	}
	// Otherwise decellerate
	else if (events->type == SDL_KEYUP)
	{
		if(events->key.keysym.sym == SDLK_RIGHT)
		{
			movingRight = false;
		}
		else if(events->key.keysym.sym == SDLK_LEFT)
		{
			movingLeft = false;
		}
		else if(events->key.keysym.sym == SDLK_z)
		{
			jumpKeyReleased = true;
		}
		else if(events->key.keysym.sym == SDLK_x)
		{
			fireButtonRelased = true;
			state = ENTITY_STATE_ALIVE;
		}
		else if(events->key.keysym.sym == SDLK_DOWN)
		{
			crouching = false;
			state = ENTITY_STATE_ALIVE;
		}
		else if(events->key.keysym.sym == SDLK_c)
		{
			boltingButtonRelased = true;

			if(bolt != NULL)
			{
				bolt->dead = true;
				bolt = NULL;
			}

			state = ENTITY_STATE_ALIVE;
		}
		
	}
}

bool HB_Player::on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis)
{

	HB_Entity::on_collision_entity(entity, moveX, moveY, axis);


	// If the player collided with an enemy
	if(entity->type == ENTITY_TYPE_ENEMY)
	{
		// Decrease its healt points
		/*hurt(1);

		hurtTimer->start();

		flags = ENTITY_FLAG_MAPONLY;
		state = ENTITY_STATE_DYING;
		dyingAnimTimer->start();*/

		dead = true;

		return true;
	}



	return false;
}

bool HB_Player::on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *axis)
{
	return HB_Entity::on_collision_map(moveX, moveY, tile, axis);
}

void HB_Player::fire()
{
	// Create a temporary pointer
	HB_Bullet *bullet;

	if(lastDirection == 0) // Right
	{
		bullet = new HB_Bullet((int)x + 31, (int)y, DIRECTION_RIGHT, mapReference);
	}
	else // Left
	{
		bullet = new HB_Bullet((int)x - 21, (int)y, DIRECTION_LEFT, mapReference);
	}

	HB_Entity::EntitiesPool.push_back(bullet); // Add the bullet to the pool

	// Set the firing again to false

	state = ENTITY_STATE_FIRING;

	firing = false;
}

void HB_Player::bolting()
{
	// Create a temporary pointer
	HB_Bolt *bolt;

	if(lastDirection == 0) // Right
	{
		bolt = new HB_Bolt((int)x + 33, (int)y, mapReference);
	}
	else // Left
	{
		bolt = new HB_Bolt((int)x - 40, (int)y, mapReference);
	}

	this->bolt = bolt;

	HB_Entity::EntitiesPool.push_back(bolt); // Add the bullet to the pool

	// Set the firing again to false

	state = ENTITY_STATE_BOLTING;

	Bolting = false;


}
