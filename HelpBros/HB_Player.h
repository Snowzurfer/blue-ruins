#pragma once

#include "HB_Global.h"
#include "HB_Entity.h"
#include "HB_AnimationManager.h"
#include "HB_Bullet.h"
#include "HB_Bolt.h"

class HB_Player : public HB_Entity
{
	private:

		/// Animation representing the player
		HB_Animation *movingAnim;

		HB_AnimationManager *animations;


		// The timer and the time that occurs when an entity is being hurt, making it 
		HB_Timer *hurtTimer;
		double hurtTime;

		// Wheter the entity has being hitten or not
		bool unactive; 

		bool fireButtonRelased;

		void fire();

		// Wheter the player is firing or not
		bool firing;

		void bolting();

		bool Bolting;

		bool boltingButtonRelased;

		

	public:
	
		/// Default constructor, calls the base class's constructor
		HB_Player(int Width, int Height, HB_Map *mapReference);

		/// Default deconstructor
		~HB_Player();

		/// Load the animation
		bool load_files();

		/// Update the player data
		void update(double *deltaTicks);
		
		/// Draw the player
		void draw(SDL_Surface *destination);
		
		/// Handle user input
		void handle_events(SDL_Event *events);

		/// Happens when this entity has collided with another one
		bool on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis);

		// Happens when this entity collides with the map for some reason
		bool on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *direction);

		// Whether to play or not the idle animation
		bool idlePlayed;

		// Wheter to play or not the falling animation
		bool fallingPlayed;

		bool moveRPlayed, moveLPlayed, jumpPlayed, crouchPlayed, boltingPlayed;

		// Whether the player is crouching or not
		bool crouching;

		HB_Bolt *bolt;
		
};