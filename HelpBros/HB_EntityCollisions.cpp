#include "HB_Entity.h"

std::vector<HB_EntityCollisions*> HB_EntityCollisions::entityColList;

HB_EntityCollisions::HB_EntityCollisions(HB_Entity *entityA, HB_Entity *entityB, float moveX, float moveY, int axis)
{
	this->entityA = entityA;
	this->entityB = entityB;
	this->moveX = moveX;
	this->moveY = moveY;
	this->axis = axis;
}


HB_EntityCollisions::~HB_EntityCollisions(void)
{

}
