#include "HB_Bullet.h"


HB_Bullet::HB_Bullet(int xPos, int yPos, int direction, HB_Map *mapReference):
	HB_Entity(20, 12, mapReference)
{
	x = xPos;
	y = yPos + 5;

	if(direction == DIRECTION_RIGHT) // If going right
	{
		movingRight = true;
		movingLeft = false;
	}
	else // If going left
	{
		movingLeft = true;
		movingRight = false;
	}

	type = ENTITY_TYPE_BULLET;

	flags = ENTITY_FLAG_NONE; // Do not make the bullet affected by gravity

	animations = new HB_AnimationManager();

	animations->add_animation("Fly", 6, 20, 12, &x, &y);

	animations->load_image("./content/gfx/Bolt_six_frames.png", "Fly");

	animations->play_animation("Fly");

	HP = 1;

	maxVelX = 17;
	xAccFactor = 17;

	state = ENTITY_STATE_ALIVE;

	dyingAnimTime = 1;

	offsetX = offsetW = 3;

}


HB_Bullet::~HB_Bullet(void)
{
	delete animations;
}

void HB_Bullet::update(double *deltaTicks)
{
	HB_Entity::update(deltaTicks);

	animations->update();
}

void HB_Bullet::draw(SDL_Surface *Dest)
{
	animations->draw(Dest);
}

bool HB_Bullet::on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis)
{

	HB_Entity::on_collision_entity(entity, moveX, moveY, axis);

	if(entity->type == ENTITY_TYPE_ENEMY)
	{
		// Hurt the bullet, making it die
		dead = true;

		return true;
	}

	return false;
}

bool HB_Bullet::on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *axis)
{

	HB_Entity::on_collision_map(moveX, moveY, tile, axis);

	// If the collision happened on the X axis
	if(*axis == AXIS_X)
	{
		dead = true;

		//tile->typeID = TILE_NONE;

		return false;
	}

	return false;
}