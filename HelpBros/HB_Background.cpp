#include "HB_Background.h"

HB_Background::HB_Background()
{
	backgroundSurface = NULL;

}

HB_Background::~HB_Background()
{
	SDL_FreeSurface(backgroundSurface);

}

bool HB_Background::load_image(std::string filePath, std::vector< std::vector< int > > backgroundData, int w, int h)
{
	int tempX = 0, tempY = 0;

	SDL_Rect tempRect;

	// Used to load each part of the background
	SDL_Surface *tempBackground = NULL;

	backgroundSurface = SDL_CreateRGBSurface(SDL_HWSURFACE, w * TILE_SIZE, h * TILE_SIZE, SCREEN_BPP, 0, 0, 0, 0); 

	// Load the various parts of the map
	for(int r = 0; r < h; r++)
	{
		for(int c = 0; c < w; c++)
		{
			// If there is a background tile which must be drawn
			if(backgroundData[r][c] != 0)
			{
				// Load that part into a temporary surface
				if((tempBackground = HB_Surface::load_image(filePath)) == NULL)
				{
					return false;
				}

				tempRect.x = 0;
				tempRect.y = 0;
				tempRect.h = tempBackground->h;
				tempRect.w = tempBackground->w;

				// Determine where to start drawing it
				tempX = c * TILE_SIZE;
				tempY = (r * TILE_SIZE) - tempBackground->h + TILE_SIZE;

				// Apply it to the correct position into the main surface
				HB_Surface::draw_surface(backgroundSurface, tempX, tempY, tempBackground, &tempRect);

				// Free the temporary surface
				SDL_FreeSurface(tempBackground);
			}
		}
	}

	return true;
}

void HB_Background::update()
{
	whatToDraw.x = HB_Camera::cameraControl.cameraBounds.x;
	whatToDraw.y = HB_Camera::cameraControl.cameraBounds.y;
	whatToDraw.w = whatToDraw.x + SCREEN_WIDTH;
	whatToDraw.h = whatToDraw.y + SCREEN_HEIGHT;
}

void HB_Background::draw(SDL_Surface *destination)
{
	HB_Surface::draw_surface(destination, 0, 0, backgroundSurface, &whatToDraw);
}