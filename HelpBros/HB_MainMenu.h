#pragma once

#include "HB_Global.h"
#include "HB_GameState.h"

enum selectedItem
{
	MENU_START = 0,
	MENU_OPTIONS = 1,
	MENU_CREDITS = 2,
	MENU_EXIT = 3
};

class HB_MainMenu : public HB_GameState
{
	public:

		// Constructor
		HB_MainMenu(void);

		// Deconstructor
		~HB_MainMenu(void);

		void handle_events(SDL_Event *Events);

		void update(double *deltaTicks);

		void draw(SDL_Surface *destination);

		bool load_files();

	private:

		// The font used for the title
		TTF_Font *font0;
		SDL_Surface *message0;

		// The font used for the menu options
		TTF_Font *font1;

		SDL_Surface *buttonUnused[4];
		SDL_Surface *buttonHighlight[4];

		SDL_Color *blue;
		SDL_Color *white;

		// The font used for the company's name
		TTF_Font *font2;
		SDL_Surface *message2;

		SDL_Color *textColor;

		// The title background
		SDL_Surface *background;

		// Determines where the user wants to go
		selectedItem selection;
		
		// Physically detect which choice has been made
		int selectionIndex;

		// Used to not make the selection go fast
		bool upPressed, downPressed, returnPressed;
		
};


