#pragma once

#include "HB_Global.h"
#include "HB_Timer.h"
#include "HB_Surface.h"
#include "HB_Font.h"

/// This class represent the console shown when needed
class HB_Console
{
	private:
		
		HB_Font *font;

		SDL_Surface *srfc;
		
		/// Pointer to the game FPS timer, used to print the actual FPS
		HB_Timer *fps;
		double lastfps;
		int cont;

		float *plx,  *ply, *velx, *vely;
		double second;


		
	public:
		
		/// Default constructor
		HB_Console(HB_Timer *fpsTimer, float *plx, float *ply, float *velx, float *vely);

		// Default deconstructor
		~HB_Console();
		
		/// Update its contents
		void update(double *deltaTicks);
		
		/// Draw its contents
		void draw(SDL_Surface *screen);		

		bool load_files();
};
