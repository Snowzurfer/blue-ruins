#include "HB_Bolt.h"


HB_Bolt::HB_Bolt(int posX, int posY, HB_Map *mapReference)
	:HB_Entity(40,12, mapReference)
{

	x = posX;
	y = posY + 5;

	animation = new HB_AnimationManager();

	animation->add_animation("Fly", 3, 40, 12, &x, &y);

	type = ENTITY_TYPE_BOLT;

	flags = ENTITY_FLAG_NONE;

	
	animation->load_image("./content/gfx/Bolt_six_frames.png", "Fly");
	
	animation->play_animation("Fly");

	state = ENTITY_STATE_ALIVE;

	HP = 1;

	dead = false;

	movingLeft = movingRight = false;

	offsetW = offsetX = 3;

	dyingAnimTime = 1000;
}


HB_Bolt::~HB_Bolt(void)
{
	delete animation;
}

void HB_Bolt::update(double *deltaTicks)
{
	HB_Entity::update(deltaTicks);

	animation->update();
}

void HB_Bolt::draw(SDL_Surface *Dest)
{
	animation->draw(Dest);
}

bool HB_Bolt::on_collision_entity(HB_Entity *entity, float *moveX, float *moveY, int *axis)
{
	//HB_Entity::on_collision_entity(entity, moveX, moveY, axis);

	if(entity->type == ENTITY_TYPE_ENEMY)
	{
		return true;
	}

	return false;
}

bool HB_Bolt::on_collision_map(float *moveX, float *moveY, HB_Tile *tile, int *axis)
{
	return false;
}

void HB_Bolt::handle_events(SDL_Event *Event)
{

}



